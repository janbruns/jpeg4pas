{$ifndef ALLPACKAGES}
{$mode objfpc}{$H+}
program fpmake;

uses fpmkunit;

Var
  T : TTarget;
  P : TPackage;
begin
  With Installer do
    begin
{$endif ALLPACKAGES}

    P:=AddPackage('jpeg2');
{$ifdef ALLPACKAGES}
    P.Directory:='jpeg2';
{$endif ALLPACKAGES}
    P.Version:='2.6.4';

    P.Author := 'Jan Bruns';
    P.License := 'almost public domain, see doc.html';
    P.HomepageURL := 'http://abnuto.de/jan/code/jpg/doc.html';
    P.Email := 'code@abnuto.de';
    P.Description := 'Another jpeg implementation (baseline, progressive, lossless, all with arbitrary bit-depths). But Huffmann, only.';

    P.SourcePath.Add('src');
    P.IncludePath.Add('src');

    T:=P.Targets.AddUnit('jpeg_parser.pas');
    with P.Targets do
      begin
        AddImplicitUnit('jpeg_misc.pas');
        AddImplicitUnit('jpeg_headerdata.pas');
        AddImplicitUnit('jpeg_huffdec.pas');
        AddImplicitUnit('jpeg_sofmarkers.pas');
        AddImplicitUnit('jpeg_huffreader.pas');
        AddImplicitUnit('jpeg_streamreader.pas');
        AddImplicitUnit('jpeg_imagecomponent.pas'); 
      end;
    T:=P.Targets.AddUnit('jpeg_streamwriter.pas');
    with P.Targets do
      begin
        AddImplicitUnit('jpeg_misc.pas');
        AddImplicitUnit('jpeg_headerdata.pas');
        AddImplicitUnit('jpeg_huffenc.pas');
        AddImplicitUnit('jpeg_sofmarkers.pas');
        AddImplicitUnit('jpeg_huffwriter.pas');
        AddImplicitUnit('jpeg_imagecomponent.pas');
      end;
    T:=P.Targets.AddUnit('jpeg_imagecomponent.pas');
    T.Dependencies.AddInclude('jpeg_dct.pas');
    with P.Targets do
      begin
        AddImplicitUnit('jpeg_headerdata.pas');
        AddImplicitUnit('jpeg_misc.pas');
        AddImplicitUnit('jpeg_pred.pas');
        AddImplicitUnit('jpeg_huffenc.pas');
        AddImplicitUnit('jpeg_huffdec.pas');
        AddImplicitUnit('jpeg_sofmarkers.pas');
      end;
    T:=P.Targets.AddUnit('jpeg_writer.pas');
    with P.Targets do
      begin
        AddImplicitUnit('jpeg_imagecomponent.pas');
        AddImplicitUnit('jpeg_streamwriter.pas');
      end;


    P.Sources.AddSrc('doc.html');
 
    P.ExamplePath.Add('examples');

    P.Targets.AddExampleProgram('testbmp2jpeg.pas');
    P.Targets.AddExampleProgram('testjpeg2bmp.pas');

    T := P.Targets.AddExampleUnit('FPReadJPEG2.pas');
    T.Dependencies.Add('fcl-image');
    T := P.Targets.AddExampleUnit('fpwritejpegLL.pas');
    T.Dependencies.Add('fcl-image');
    T := P.Targets.AddExampleProgram('draw.pas');
    T.Dependencies.Add('fcl-image');
    


{$ifndef ALLPACKAGES}
    Run;
    end;
end.
{$endif ALLPACKAGES}

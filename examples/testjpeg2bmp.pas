{$mode objfpc}
uses jpeg_parser,jpeg_imagecomponent,jpeg_streamwriter,classes,sysutils,bmp;


type
Tmyjpegparser = class(Tjpegparser)
  procedure handle_EOI(c : byte); override;
end;

procedure Tmyjpegparser.handle_EOI(c : byte);
begin
  writeln('EOI');
  done := true;
end;


procedure paintcompo(c : Tjpeg_imagecomponent; const fi : Tfloatimg; col,xres,yres : longint);
var x,y,x1,y1,dx,dy,sx,sy,sx1,sy1,xr2,yr2 : longint; dst : Pfloat; q,w : single;
begin
  sx := 1; while (sx*c.xres<xres) do inc(sx);
  sy := 1; while (sy*c.yres<yres) do inc(sy);
writeln('compo ',col,' sx=',sx,' sy=',sy,' bw=$',hexstr(c.brightwhite,4));
  sx1 := sx-1;
  sy1 := sy-1;
  dst := @fi.dat[col];
  xr2 := c.xres-1;
  yr2 := c.yres-1;
  q := 1/c.brightwhite;
  for y := 0 to yr2 do begin
    for x := 0 to xr2 do begin
      w := q * c.get_line_pointer(x,y)^;
      for y1 := 0 to sy1 do begin
        for x1 := 0 to sx1 do begin
          dy := y*sy+y1;
          dx := x*sx+x1;
          if (dx<xres)and(dy<yres) then begin
            dst[(dy*xres+dx)*3] := w;
          end;
        end;
      end;
    end;
  end;
writeln('donepaint');
end;

procedure JFIF_YCbCr2sRGB(var a1,a2,a3 : single);
var r,g,b,y,cb,cr : single;
begin
  Y := a1;
  cb := a2;
  cr := a3;
  R  := Y + 1.402*(Cr-0.5);
  G  := Y - 0.34414*(Cb-0.5) -0.71414*(Cr-0.5);
  B  := Y + 1.772*(Cb-0.5);
  a1 := r; 
  a2 := g;
  a3 := b;
end;




var
jpg : Tmyjpegparser;
s : TMemoryStream;
t1,t2 : double;
i,x,y : longint;
carr : Tcompoarr;
fi : Tfloatimg;
xres,yres : longint;

BEGIN
t1 := now();
  if (paramcount>=2) then begin
  end else begin
    writeln('usage:  testjpeg2bmp src.jpg dst.bmp');
  end;

  s := TMemoryStream.create();
  s.LoadFromFile(paramstr(1));

  jpg := Tmyjpegparser.create();
  jpg.parse(s);
  jpg.get_all_components(carr);
  t2 := now();
  t2 := (t2-t1)*3600*24;
  writeln('time after decoding:',t2);
//halt(0);
  InitImg(fi);
  jpg.getres(xres,yres); 
  allocImg(fi,xres,yres);

  for i := 0 to high(carr) do begin
    if (i>2) then break;
    carr[i].postprocess();
    paintcompo(carr[i],fi,i,xres,yres);    
  end;

  if jpg.sofh.is_dct then begin
    for y := 0 to fi.yres-1 do begin
      for x := 0 to fi.xres-1 do begin
        JFIF_YCbCr2sRGB(fi.dat[(y*fi.xres+x)*3],fi.dat[(y*fi.xres+x)*3+1],fi.dat[(y*fi.xres+x)*3+2]);
        { write to jpeg component memory }
      end;
    end;
  end;
  saveBMP(paramstr(2),fi);

  releaseImg(fi);

  jpg.destroy();
  s.destroy();

  t2 := now();
  t2 := (t2-t1)*3600*24;
  writeln('time after all:',t2);

END.
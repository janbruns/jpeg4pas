{$mode objfpc}{$h+}
program Drawing;

uses classes, sysutils,
     FPImage, FPCanvas, FPImgCanv, ftFont,
     fpwritejpegLL, FPReadPNG, FPReadJPEG2;

const
  MyColor : TFPColor = (Red: $7FFF; Green: $0000; Blue: $FFFF; Alpha: alphaOpaque);

var startuptime : double;

procedure DoDraw;
var canvas : TFPcustomCAnvas;
    ci, image : TFPCustomImage;
    writer : TFPCustomImageWriter;
    reader : TFPReaderJPEG2;
    markers : Tjpegmarkerstream;
begin
  image := TFPMemoryImage.Create (100,100);
  ci := TFPMemoryImage.Create (0,0);
  Canvas := TFPImageCanvas.Create (image);
  Writer := TFPwriterJPEGLL.Create;
  reader := TFPReaderJPEG2.Create;

  try
{
    ci.LoadFromFile (paramstr(1), reader);
    writeln('img (',ci.width,'x',ci.height,') loaded after ',(now()-startuptime)*3600*24,'s');
    markers := reader.first_marker;
    while not(markers=nil) do begin
      writeln('there was an APP',markers.markertype and 15,' marker of length ',markers.size);
      markers := markers.next;
    end;
    writeln('"the reader did color convert" is ',reader.did_colorconvert);
    writeln('the image has ',reader.img_components,' of ',reader.jpg_components,' components found in the file.');
}
    ci.LoadFromFile (paramstr(1) );

    with Canvas as TFPImageCanvas do begin

      StretchDraw(1,1,98,98,ci);

      writeln('img (',ci.width,'x',ci.height,') strechdrawed after ',(now()-startuptime)*3600*24,'s');

      pen.mode := pmCopy;
      pen.style := psSolid;
      pen.width := 1;
      pen.FPColor := colred;
      with pen.FPColor do
        red := red div 4;
      Ellipse (10,10, 90,90);


      pen.style := psDashDot;
      pen.FPColor := colred;
      HashWidth := 10;
      Ellipse (10,10, 90,90);

      with pen.FPColor do
        begin
        red := red div 2;
        green := red div 4;
        blue := green;
        end;
      pen.style := psSolid;
      RelativeBrushImage := true;

      brush.style := bsSolid;
      brush.FPColor := MyColor;
      pen.style := psSolid;
      pen.width := 3;
      pen.FPColor := colSilver;
      ellipse (30,35, 70,65);

      pen.width := 1;
      pen.FPColor := colCyan;
      ellipseC (50,50, 1,1);
    end;
    writeln ('Saving to inspect !');
    image.SaveToFile ('DrawTest.jpg', writer);
    writeln('save small image done after ',(now()-startuptime)*3600*24,'s');
  finally
    Canvas.Free;
    image.Free;
    writer.Free;
    reader.destroy();
    ci.free;
  end;
end;

begin
  startuptime := now();
  DoDraw;
end.

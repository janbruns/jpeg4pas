{$mode objfpc}

unit FPReadJPEG2;



interface

uses
  Classes, SysUtils, math, FPImage;

type

Tjpegmarkerstream = class(TMemoryStream)
  markertype : byte;
  next : Tjpegmarkerstream;
end;

TFPReaderJPEG2 = class(TFPCustomImageReader)
  constructor create(); override;
  destructor destroy(); override;

  protected
  procedure InternalRead(s : TStream; Img: TFPCustomImage); override;
  function  InternalCheck(s : TStream): boolean; override;

  public
  first_marker : Tjpegmarkerstream;

  { enable the very freequently used sRGB<->YCbCr conversion.

    Can be set to false, to let color managment-aware callers
    do their own calculations. In this case, the output will
    be the pure JPEG encoding after DCT/UNDIFF, scaled to 
    match TFPimage convention. }
  enable_colorconvert : boolean; 

  { and tell if this action really was applied }
  did_colorconvert : boolean;

  { the number of image components the file had,  
    and the number of components now available in
    the img }
  jpg_components : longint;
  img_components : longint;

  private
  r : TRect;
  last_marker : Tjpegmarkerstream;
end;


implementation
uses jpeg_parser,jpeg_imagecomponent;

var
startuptime : double;

type
Tmyjpegparser = class(Tjpegparser)
  procedure handle_EOI(c : byte); override;
  procedure handle_APP(c : byte); override;
  procedure handle_SOF(c : byte); override;

  { don't generate hufftabs on checkonly-scans }
  procedure handle_DHT(c : byte); override;

  constructor create();
  
  procedure append_marker_to_reader(s : Tjpegmarkerstream);

  protected
  checkonly,
  take_markers,
  seen_jfif : boolean;


  public
  reader : TFPReaderJPEG2;
end;

constructor Tmyjpegparser.create();
begin
  inherited create();
  seen_jfif := false;
end;

procedure Tmyjpegparser.handle_SOF(c : byte); 
begin
  if checkonly then begin
    done := true;
    seek(readlen(),soFromCurrent);
    c := c and 15;
    { might also support hierachical modes 5,6,7 in future }
    if (c>3) then TjpegparserExcpt.create('unsupported jpeg frame.');
  end else begin
    inherited handle_SOF(c);
  end;
end;

{ don't generate hufftabs on checkonly-scans }
procedure Tmyjpegparser.handle_DHT(c : byte); 
var len : longint;
begin
  if checkonly then begin
    len := readlen();
    seek(len,soFromCurrent);
  end else begin
    inherited handle_DHT(c);
  end;
end;


procedure Tmyjpegparser.handle_EOI(c : byte);
begin
  done := true;
//writeln('EOI');
end;

procedure Tmyjpegparser.append_marker_to_reader(s : Tjpegmarkerstream);
begin
  s.next := nil;
  if (reader.last_marker=nil) then begin
    reader.last_marker := s;
    reader.first_marker := s;
  end else begin
    reader.last_marker.next := s;
    reader.last_marker := s;
  end;
end;

procedure Tmyjpegparser.handle_APP(c : byte);
var len : longint; buff : ansistring; m : Tjpegmarkerstream;
begin
  len := readlen();
  if (len>0) then begin
    setlength(buff,len);
    readbuffer(buff[1],len);
    if take_markers and not(reader=nil) then begin
      m := Tjpegmarkerstream.create();
      m.writebuffer(buff[1],len);
      m.markertype := c;
      append_marker_to_reader(m);
    end;
    c := c and 15;
    if (c=0)and(len>4) then begin // APP0 might be JFIF
      if (copy(buff,1,5)='JFIF'#0) then seen_jfif := true;
    end;
  end;
end;


type
TsomeFPMemoryImage = class(TFPMemoryImage)
  function getdatapointer() : pointer;
end;

function TsomeFPMemoryImage.getdatapointer() : pointer;
begin
  getdatapointer := FData;
end;


{ for now, a float version }
procedure JFIF_YCbCr(p : Pword);
var y,cb,cr : longint; r,g,b : longint;
begin
  y := p^;
  cb := p[1];
  cr := p[2];
  y := y * 16384;
  cr := cr -32768;
  cb := cb -32768;
  r := Y + 22970*Cr;
  g := Y - 5638*Cb -11700*Cr;
  b := Y + 29032*Cb;
  if (r<0) then r := 0;
  if (g<0) then g := 0;
  if (b<0) then b := 0;
  r := r shr 14;
  g := g shr 14;
  b := b shr 14;
  if (r>65535) then r := 65535;
  if (g>65535) then g := 65535;
  if (b>65535) then b := 65535;
  p[0] := r;
  p[1] := g;
  p[2] := b;
end;


procedure TFPReaderJPEG2.InternalRead(s: TStream; Img: TFPCustomImage);
var jpg : Tmyjpegparser; xres,yres,i,x,y,x2,xs,ys,cc : longint; bws : single;
    carr : Tcompoarr; psrc,pdst : Pword; c : Tjpeg_imagecomponent;
    img2 : TsomeFPMemoryImage; b : boolean; p2 : ^TFPColor;
begin
  r.Left:=0; r.Top:=0; r.Right:=0; r.Bottom:=0;
  b := true;
  Progress(psStarting,0,false,r,'read jpeg-bitstream',b);
  if not(b) then exit;

  setlength(carr,0);
  jpg := Tmyjpegparser.create();
  jpg.reader := self;
  jpg.take_markers := true;
  jpg.seen_jfif := false;
  jpg.checkonly := false;
  try
    jpg.parse(s);
    b := false;
  finally
    if b then begin
      jpg.destroy();
    end else b := true;
  end;
  jpg.get_all_components(carr);
  jpg.getres(xres,yres);

  jpg_components := high(carr)+1;
  img_components := min(jpg_components,4);

  Img2 := TsomeFPMemoryImage.create(0,0);
  Img2.UsePalette := false;
  Img2.SetSize(xres,yres);

  { assume this was enough to ensure Img is now 8 bytes per pixel,
    and is now internally exactly xres wide, in the data-layout. }



  Progress(psRunning,10,false,r,'process components 1/2',b);

  { do DCT and convert to scanline layout }
  cc := min(high(carr),3); // don't process more than 4 components
  for i := 0 to high(carr) do begin
    carr[i].postprocess();
  end;

  Progress(psRunning,60,false,r,'process components 2/2',b);

  { because JFIF defines subsampled components do be
    on center-positions, normally out of the reference raster,
    it doesn't make much sense to use anything else than 
    nearest point sampling, except probably for odd scales,
    that are normally not used. }

  for i := 0 to cc do begin
    c := carr[i];
    { calc the sub-sampling factor }
    x := 0; xs := 0; while (x<xres) do begin x := x + c.xres; inc(xs); end;
    y := 0; ys := 0; while (y<yres) do begin y := y + c.yres; inc(ys); end;
    for y := 0 to yres-1 do begin
      psrc := c.get_line_pointer(0,y div ys);
      pdst := img2.getdatapointer();
      pdst := @pdst[y*xres*4+i];
      x2 := xs;
      for x := xres-1 downto 0 do begin // won't care about x
        pdst^ := psrc^;
        inc(pdst,4);
        dec(x2);
        if (x2=0) then begin
          x2 := xs;
          inc(psrc);
        end;
      end;
    end;
  end;

  { maybe we want white=brightwhite }
  for i := 0 to cc do begin
    c := carr[i];
    if not(c.brightwhite=$FFFF) then begin
      bws := 65535.0 / c.brightwhite;
      for y := 0 to yres-1 do begin
        pdst := img2.getdatapointer();
        pdst := @pdst[y*xres*4+i];
        for x := xres-1 downto 0 do begin // won't care about x
          pdst^ := round(bws*pdst^);
          inc(pdst,4);
        end;
      end;
    end;
  end;

  { if there are no greenish or blueish compos, copy red}
  for i := cc+1 to 2 do begin
    for y := 0 to yres-1 do begin
      pdst := img2.getdatapointer();
      psrc := @pdst[y*xres*4];
      pdst := @pdst[y*xres*4+i];
      for x := xres-1 downto 0 do begin // won't care about x
        pdst^ := psrc^;
        inc(pdst,4);
        inc(psrc,4);
      end;
    end;
  end;
  if (img_components<3) then img_components := 3;

  { leave out alpha-init step, so if there was no data read,
    TFPCustomImage was responsible for filling in whatever it 
    expects in such a case.}

  { finally, if there were 3 compos, the data was probably YCbCr.
    for now, do it here }
  if enable_colorconvert then begin
    if (cc=2)and(jpg.seen_jfif or jpg.sofh.is_DCT) then begin
      for y := 0 to yres-1 do begin
        pdst := img2.getdatapointer();
        pdst := @pdst[y*xres*4];
        for x := xres-1 downto 0 do begin // won't care about x
          JFIF_YCbCr(pdst);
          inc(pdst,4);
        end;
      end;
      did_colorconvert := true;
    end;
  end;
  jpg.destroy();

  Progress(psRunning,100,false,r,'finalize',b);
  Img.SetSize(0,0);
  Img.UsePalette := false;
  Img.SetSize(xres,yres);
//  writeln('Image almost done after ',(now()-startuptime)*3600*24,'s');
  p2 := img2.getdatapointer();
  for y := 0 to yres-1 do begin
    for x := xres-1 downto 0 do begin
      img.colors[x,y] := p2^;
      inc(p2);
    end;
  end;
  img2.destroy();
  Progress(psEnding,100,false,r,'',b);
end;


function  TFPReaderJPEG2.InternalCheck(s : TStream): boolean;
var b : boolean; jpg : Tmyjpegparser;
begin
  InternalCheck := false;
  jpg := Tmyjpegparser.create();
  jpg.reader := self;
  jpg.take_markers := false; // currently unused
  jpg.seen_jfif := false;
  jpg.checkonly := true;
  b := true;
  try
    jpg.parse(s);
    b := false;
  finally
    if b then begin
      jpg.destroy();
    end else b := true;
  end;
  jpg.destroy();
  InternalCheck :=true;
end;



constructor TFPReaderJPEG2.Create;
begin
  inherited Create();
  first_marker := nil;
  last_marker := nil;
  enable_colorconvert := true;
  did_colorconvert := false;
end;

destructor TFPReaderJPEG2.Destroy;
var m,m2 : Tjpegmarkerstream;
begin
  m := first_marker;
  while not(m=nil) do begin
    m2 := m.next;
    m.destroy();
    m := m2;
  end;
  inherited Destroy();
end;

initialization
  startuptime := now();
  ImageHandlers.RegisterImageReader('JPEG image', 'jpg;jpeg', TFPReaderJPEG2);
end.

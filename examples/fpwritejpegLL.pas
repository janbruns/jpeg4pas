{$mode objfpc}{$H+}
unit fpwritejpegLL;



interface

uses
  Classes, SysUtils, FPImage, jpeg_imagecomponent,jpeg_writer,jpeg_streamwriter;

type

TFPWriterJPEGll = class(TFPCustomImageWriter)
  protected
  procedure InternalWrite(s : TStream; Img: TFPCustomImage); override;

  public
  constructor create(); override;
  destructor destroy(); override;
end;

implementation

procedure TFPWriterJPEGll.InternalWrite(s : TStream; Img: TFPCustomImage); 
var col : TFPColor; i,x,y : longint; p0,p1,p2,p3 : Pimgdataunit;
    ms : TMemoryStream; jpg : Tjpegwriter; carr : Tcompoarr;
begin
  { first we need some Tjpeg_imagecomponents }
  setlength(carr,4);
  for i := 0 to 3 do begin
    carr[i] := Tjpeg_imagecomponent.create();
    { this should be documented elsewhere ...}
    carr[i].recalc_area(Img.width,Img.height,1,1,1,1,true,imgorg_scanlines);
    carr[i].makeup_area(nil,false); // alocate auto-managed memory for us
    carr[i].jpeg_precision := 16;
    carr[i].brightwhite := $FFFF;
  end;

  { move the image data to the jpeg components }
  for y := 0 to img.height-1 do begin
    p0 := carr[0].get_line_pointer(0,y);
    p1 := carr[1].get_line_pointer(0,y);
    p2 := carr[2].get_line_pointer(0,y);
    p3 := carr[3].get_line_pointer(0,y);
    for x := 0 to Img.width-1 do begin
      { write to jpeg component memory }
      col := Img.colors[x,y];
      p0[x] := col.red;
      p1[x] := col.green;
      p2[x] := col.blue;
      p3[x] := col.alpha;
    end;
  end;
  ms := TMemoryStream.create();
  jpg := Tjpegwriter.create();
  jpg.setStream(ms);
  jpg.do_LL(carr);
  s.copyFrom(ms,0);
  ms.destroy();
  jpg.destroy();
  for i := 0 to high(carr) do begin
    carr[i].destroy();
  end;
end;

constructor TFPWriterJPEGll.create(); 
begin
end;

destructor TFPWriterJPEGll.destroy();
begin
end;






initialization
  ImageHandlers.RegisterImageWriter('Lossless JPEG', 'jpg;jpeg', TFPWriterJPEGll);
end.

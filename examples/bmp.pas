UNIT bmp;

INTERFACE


TYPE
  BITMAPFILEHEADER = packed record
    bfType : Word;
    bfSize : DWord;
    bfReserved1 : Word;
    bfReserved2 : Word;
    bfOffBits : DWord;
  end;
  LONG = longint;

  BITMAPINFOHEADER = record
    biSize : DWORD;
    biWidth : LONG;
    biHeight : LONG;
    biPlanes : WORD;
    biBitCount : WORD;
    biCompression : DWORD;
    biSizeImage : DWORD;
    biXPelsPerMeter : LONG;
    biYPelsPerMeter : LONG;
    biClrUsed : DWORD;
    biClrImportant : DWORD;
  end;
  LPBITMAPINFOHEADER = ^BITMAPINFOHEADER;
  TBITMAPINFOHEADER = BITMAPINFOHEADER;
  PBITMAPINFOHEADER = ^BITMAPINFOHEADER;

  Tfloat = single;
//  TFloat = extended;
  PFloat = ^Tfloat;
  Tfloatimg = record
    xres,yres,mem  : longint;
    dat : Pfloat;    
  end;


PROCEDURE InitImg(VAR img : Tfloatimg);
PROCEDURE releaseImg(VAR img : Tfloatimg);
PROCEDURE allocImg(VAR img : Tfloatimg; xres,yres : longint);

FUNCTION loadBMP(fn : ansistring; var img : Tfloatimg) : boolean;
PROCEDURE saveBMP(fn : ansistring; img : Tfloatimg);


IMPLEMENTATION
uses math;


FUNCTION loadBMP(fn : ansistring; var img : Tfloatimg) : boolean;
VAR f : FILE; bmi : BITMAPINFOHEADER; bmpfh : BITMAPFILEHEADER; x,y,iy,bpl : longint; p : pbyte;
BEGIN
  loadBMP := false;
  img.xres := 0;
  img.yres := 0;
  img.mem  := 0;
  img.dat  := nil;
  assign(f,fn);
{$I-}
  reset(f,1);
  if not(ioresult=0) then exit;
  blockread(f,bmpfh,sizeof(bmpfh));
  if ((filesize(f) -sizeof(bmpfh) -sizeof(bmi))>0) then begin
    blockread(f,bmi,sizeof(bmi));
    img.xres := bmi.biWidth;
    img.yres := bmi.biHeight;
    img.mem := img.xres*img.yres*3*sizeof(Tfloat);
    getmem(img.dat,img.mem);
    getmem(p,bmi.biSizeImage);
    seek(f,bmpfh.bfOffBits);
    blockread(f,p^,bmi.biSizeImage,x);
    bpl := bmi.biSizeImage DIV bmi.biHeight;
    iy := 0;
    for y := img.yres-1 downto 0 do begin
      for x := 0 to img.xres-1 do begin
        img.dat[3*(iy*img.xres + x)+2] := p[3*x + y*bpl +0]/255;
        img.dat[3*(iy*img.xres + x)+1] := p[3*x + y*bpl +1]/255;
        img.dat[3*(iy*img.xres + x)+0] := p[3*x + y*bpl +2]/255;
      end;
      iy := iy + 1;
    end;
   end;
  loadBMP := true;
  freemem(p,bmi.biSizeImage);
  close(f);
{$I+}

END;

PROCEDURE saveBMP(fn : ansistring; img : Tfloatimg);
VAR f : FILE; bmi : BITMAPINFOHEADER; bmpfh : BITMAPFILEHEADER; x,y,iy,bpl : longint; p : pbyte;
c1,c2,c3 : extended;
BEGIN
  bpl := img.xres*3;
  while (bpl mod 4)>0 do bpl += 3;
  getmem(p,img.yres*bpl);
  c1 := 0; c2 := 0; c3 := 0;
  iy := 0;
  for y := img.yres-1 downto 0 do begin
    for x := 0 to img.xres-1 do begin
      c1 := img.dat[ 3*(y*img.xres + x)+2 ];
      c2 := img.dat[ 3*(y*img.xres + x)+1 ];
      c3 := img.dat[ 3*(y*img.xres + x)+0 ];
      p[ iy*bpl +3*x +0 ] := round(min(max(c1 *255,0),255));
      p[ iy*bpl +3*x +1 ] := round(min(max(c2 *255,0),255));
      p[ iy*bpl +3*x +2 ] := round(min(max(c3 *255,0),255));
    end;
    iy := iy + 1;
  end;
  fillbyte(bmi,sizeof(bmi),0);
  bmi.biWidth  := bpl div 3;
  bmi.biHeight := img.yres;
  bmi.bisize := sizeof(bmi);
  bmi.biBitCount := 24;
  bmi.biPlanes := 1;
  bmi.biCompression := 0;
  bmi.biSizeImage := img.yres*bpl;
  bmpfh.bfType := 19778;  //'BM';
  bmpfh.bfSize := sizeof(bmi)+sizeof(bmpfh)+bpl*bmi.biHeight;
  bmpfh.bfReserved1 := 0;
  bmpfh.bfReserved2 := 0;
  bmpfh.bfOffBits := (sizeof(bmpfh)+sizeof(bmi));
  assign(f,fn);
  rewrite(f,1);
  blockwrite(f,bmpfh,sizeof(bmpfh));
  blockwrite(f,bmi  ,sizeof(bmi  ));
  blockwrite(f,p[0],bmi.biWidth*bmi.biHeight*3);
  close(f);
  freemem(p,img.yres*bpl);
END;

PROCEDURE InitImg(VAR img : Tfloatimg);
BEGIN
  img.mem := 0;
  img.xres := 0;
  img.yres := 0;
  img.dat := nil;
END;

PROCEDURE releaseImg(VAR img : Tfloatimg);
BEGIN
  if (img.mem>0) then freemem(img.dat,img.mem);
  initImg(img);
END;

PROCEDURE allocImg(VAR img : Tfloatimg; xres,yres : longint);
BEGIN
  img.xres := xres;
  img.yres := yres;
  img.mem  := xres*yres*sizeof(Pfloat^)*3;
  getmem(img.dat,img.mem); 
END;


BEGIN
END.


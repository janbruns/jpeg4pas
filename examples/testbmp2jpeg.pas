{$mode objfpc}
uses bmp,jpeg_imagecomponent,jpeg_writer,jpeg_streamwriter,classes,sysutils;

{ A simple axample application that
  converts 24bpp bmp-files to jpeg.
}




{ Only done for DCT-based process here.

  The scope of this example application 
  doesn't cover color-space conversions.
}
procedure JFIF_sRGB2YCbCr(var a1,a2,a3 : single);
var r,g,b,y,cb,cr : single;
begin
  r := a1;
  g := a2;
  b := a3;
  y := 0.299*R + 0.587*G + 0.114*B;
  cb := -0.1687*R -0.3313*G +0.5*B + 0.5;
  cr := 0.5*R -0.4187*G -0.0813*B + 0.5;
  a1 := y; 
  a2 := cb;
  a3 := cr;
end;



{ Quantization table

  This is, where a quality-setting normally would 
  come into play.

  Tquantitab is an array[64] of uint16.

  Larger values represent lower quality and smaller files.
  
  I jpeg_precision changes, the values need to be scaled
  by 2^(newprec-oldprec) to take the same effect.

  Order is non-zigzag, simple dct block layout.
  For 8 Bit baseline, values must be in 1..255.
}
procedure define_a_qtab(c : Tjpeg_imagecomponent);
var q : Tquantitab; i : longint;
begin
  for i := 0 to 63 do begin
    q[i] := 8;//+random(100);
  end;
  c.take_qtab(@q);
end;


VAR
fi : Tfloatimg;
i,j,x,y : longint;
carr : Tcompoarr;
p0,p1,p2 : Pimgdataunit;
fs : TFileStream;
ms : TMemoryStream;
jpg : Tjpegwriter;
dct : boolean;

const 
{ these constants are more or less unrelated.

  Currently, Tjpegwriter can only put out 
  + baseline DCT-based (limits to 8 Bit, so prec must be = 8 here, yet)
  + Lossless (prec can be 2..16 Bits)

  The DCT-based process auto-adopts bw (brightwhite).

  The lossless process however expects bw to be the
  precision mask (2^prec -1). Or... no, to be more precise,
  it just ignores bw totallly, and even doesn't care very
  much about jpeg_prec (that has some influence on the initial
  prediction on the top-left-most pixel, and of course,
  tools reading the file might not expect to see values
  outside the precision range. it's undefined how they'll
  deal with it=. So the one that expects something about
  this constant for the lossless case is probably you.

}
jpeg_prec = 8;
bw = 255;


BEGIN
  dct := true;
  if (upcase(paramstr(3))='LL') then dct := false;
  if (paramcount>=2) then begin
    if loadbmp(paramstr(1),fi) then begin
      writeln('Loaded BMP (',fi.xres,'x',fi.yres,')');
      if ((fi.xres mod 4)>0) then writeln('warn: The bmp-importer has problems with widths not a multiple of 4.');


      { first we need some Tjpeg_imagecomponent }
      setlength(carr,3);
      for i := 0 to 2 do begin
        carr[i] := Tjpeg_imagecomponent.create();
        { this should be documented elsewhere ...}
        carr[i].recalc_area(fi.xres,fi.yres,1,1,1,1,true,imgorg_scanlines);
        carr[i].makeup_area(nil,false); // alocate auto-managed memory for us
        carr[i].jpeg_precision := jpeg_prec;
        carr[i].brightwhite := bw;
        define_a_qtab(carr[i]);
      end;

      { move the image data to the jpeg components }
      for y := 0 to fi.yres-1 do begin
        p0 := carr[0].get_line_pointer(0,y);
        p1 := carr[1].get_line_pointer(0,y);
        p2 := carr[2].get_line_pointer(0,y);
        for x := 0 to fi.xres-1 do begin
          { maybe do some color-space conversion to make other tools 
            show the colors one might (but shouldn't) expect }
          if dct then JFIF_sRGB2YCbCr(fi.dat[(y*fi.xres+x)*3],fi.dat[(y*fi.xres+x)*3+1],fi.dat[(y*fi.xres+x)*3+2]);
          { write to jpeg component memory }
          p0[x] := round(fi.dat[(y*fi.xres+x)*3  ]*bw);
          p1[x] := round(fi.dat[(y*fi.xres+x)*3+1]*bw);
          p2[x] := round(fi.dat[(y*fi.xres+x)*3+2]*bw);
        end;
      end;
      if dct then begin
        write('DCT');
        for i := 0 to high(carr) do begin
          { dct doesn't like scanlines as much as we do }
          carr[i].ConvertScanlineToDCT();
          carr[i].dofdct();
          write('.');
        end;
        writeln;
      end;

      fs := TFileStream.Create(paramstr(2),fmCreate);
      ms := TMemoryStream.create();
      jpg := Tjpegwriter.create();
      jpg.setStream(ms);
      if dct 
      then  jpg.do_baseline(carr)
      else  jpg.do_LL(carr);
      fs.copyFrom(ms,0);
      fs.destroy();
      ms.destroy();
      jpg.destroy();
      releaseImg(fi);
      for i := 0 to 2 do carr[i].destroy();
      writeln('done. See result in ',paramstr(2));
    end else begin
      writeln('Problems loading bmp.');
    end;
  end else begin
    writeln('usage dct: testbmp2jpeg src.bmp dst.jpg');
    writeln('lossless : testbmp2jpeg src.bmp dst.jpg LL');
  end;
END.

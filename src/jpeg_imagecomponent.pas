{$mode objfpc}
unit jpeg_imagecomponent;

{ About some image data processing.

  Most of these things could (but shouldn't) have 
  been coded directly into the bitstream coding process.

  This way, it'll be easier to move nearly user-preference
  parts of computations elsewhere. Also, jpeg's
  progressive modes of operation might touch data-items
  multiple times (even sometimes with reversed direction
  of data-flow).
 

  update: added an integer iDCT. This version on my computer
  suprisingly isn't faster than the float version, so it isn't 
  enabled by default. But maybe it's helpful on systems with
  slow Floating Point (see postprocess() to enable it).
}

interface
uses jpeg_huffdec,jpeg_huffenc,jpeg_misc,sysutils;

const
LL_PRED_AUTOCHOOSE_BEST = -1;


type
Timgdataorganization = 
  ( imgorg_unspecified,
    imgorg_DCT_ZZ,
    imgorg_DCT,
    imgorg_scanlines
  );
Timgdataunit = word;
Pimgdataunit = ^Timgdataunit;
Tquantitab = array[0..63] of Timgdataunit;
Pquantitab = ^Tquantitab;

Tjpeg_imagecomponent = class


  { calc the pointer into the given data organization
  }
  function get_line_pointer(lx,ly : longint) : Pimgdataunit;
  function get_DCT_ZZ_block_pointer(bx,by,bo : longint) : Pimgdataunit;
  function get_DCT_block_pointer(bx,by,bo : longint) : Pimgdataunit;

  { convert between data organizations
  }
  procedure zigzagify();
  procedure unzigzagify();
  procedure ConvertDCTtoScanline();
  procedure ConvertScanlineToDCT();
  procedure ConvertDCTScanline();

  procedure recalc_area(x,y,H,V,mH,mV : longint; blocky : boolean; o : Timgdataorganization);
  procedure makeup_area(p : pointer; keepondestroy : boolean);

  procedure take_qtab(p : Pquantitab);

  procedure do_undiff();
  procedure do_diff(pr : longint);

  constructor create();
  destructor destroy(); override;


  procedure doINTidct();
  procedure doidct();
  procedure dofdct();


  procedure fill_unused_imgarea(dct : boolean);
  procedure postprocess();


  protected

  procedure ensure_org(o : Timgdataorganization);
  procedure ensure_blocky();
  function get_block_pointer(bx,by,bo : longint) : Pimgdataunit;

  procedure do_diff17(psrc : Pimgdataunit; pr : longint);
  function compute_lensum() : qword;




  public
  imgorg : Timgdataorganization;
  xres,yres : longint; // user-resolution in pixel
  xmem,ymem : longint; // memory in pixel

  memtotal,memallocated : longint;  // memory in bytes

  blocksperrow,        // = xmem div 8, always
  blockrows : longint; //   

  noninterleaved_mcusH,           // for single component DCT scans, the
  noninterleaved_mcusV : longint; // number of blocks per full axis


  scanH,scanV : longint; // for interleaved scans, num of blocks/pixel per MCU

  blockcount : longint;

  { brightwhite: after decoding, the maximum value that the bitstream 
    might have encoded without violating rules.

    For DCT-based process, the idct impemented here sets it to $FFFF
    (or something near that value in future), instead of LSB-adjusing
    the decoded data. The reason simply is I want to keep some more 
    bits of precision (for float YCbCr=>RGB conversion, for example).

    For lossless process however, it'll be more precise to keep it
    LSB-adjusted. So if it was effective 12 Bits LL, brightwhite=$0FFF.

  }
  brightwhite : word; 

  private
  imgdat : Pimgdataunit;
  lastvalidimgdataunit : longint;

  public
  { mostly unrelated things that could be private to the bitstreamer }
  nam,qtabid,
  jpeg_precision,jpeg_pred,jpeg_pt,jpeg_rsti : longint;
  actab,dctab   : Thuffdec;
  wactab,wdctab : Thuffenc;
  lastDC : Timgdataunit;
  is_in_scan, blockyok,keeponkill,has_quantitab : boolean;
  quantitab : Tquantitab; 
end;



procedure unzigzagify_block(p,temp : Pimgdataunit);
procedure zigzagify_block(p,temp : Pimgdataunit);


type 
TjpegimageExcpt = TjpegExcpt;
Tcompoarr = array of Tjpeg_imagecomponent;


implementation
uses jpeg_sofmarkers,jpeg_headerdata,jpeg_pred,math;



type
Tzigzagpattern = array[0..63] of longint;

type
Tsstr15 = string[15];

const
zigzagpattern : Tzigzagpattern = (
 0,  1,  5,  6, 14, 15, 27, 28,
 2,  4,  7, 13, 16, 26, 29, 42,
 3,  8, 12, 17, 25, 30, 41, 43,
 9, 11, 18, 24, 31, 40, 44, 53,
10, 19, 23, 32, 39, 45, 52, 54,
20, 22, 33, 38, 46, 51, 55, 60,
21, 34, 37, 47, 50, 56, 59, 61,
35, 36, 48, 49, 57, 58, 62, 63
);


function Tjpeg_imagecomponent.get_block_pointer(bx,by,bo : longint) : Pimgdataunit;
var a : longint;
begin
  a := (blocksperrow*by + bx)*64 + bo;
  if ((a<0)or(a>lastvalidimgdataunit)) then begin
    raise TjpegimageExcpt.create('img position out of bounds.');
  end;
  get_block_pointer := @imgdat[a];
end;

function Tjpeg_imagecomponent.get_DCT_ZZ_block_pointer(bx,by,bo : longint) : Pimgdataunit;
begin
  ensure_org(imgorg_DCT_ZZ);
  ensure_blocky();
  get_DCT_ZZ_block_pointer := get_block_pointer(bx,by,bo);
end;

function Tjpeg_imagecomponent.get_DCT_block_pointer(bx,by,bo : longint) : Pimgdataunit;
begin
  ensure_org(imgorg_DCT);
  ensure_blocky();
  get_DCT_block_pointer := get_block_pointer(bx,by,bo);
end;

function Tjpeg_imagecomponent.get_line_pointer(lx,ly : longint) : Pimgdataunit;
var a : longint;
begin
  ensure_org(imgorg_scanlines);
  a := ly*xmem + lx;
  if ((a<0)or(a>lastvalidimgdataunit)) then raise TjpegimageExcpt.create('requested scan line position out of range ('+inttostr(lx)+','+inttostr(ly)+')');
  get_line_pointer := @imgdat[a];
end;



procedure Tjpeg_imagecomponent.ensure_blocky();
begin
  if not(blockyok) then raise TjpegimageExcpt.create('compo wasn''t prepared for DCT-based process.');
end;

procedure Tjpeg_imagecomponent.ensure_org(o : Timgdataorganization);
begin
  if not(imgorg=o) then raise TjpegimageExcpt.create('wrong organization.');
end;

procedure unzigzagify_block(p,temp : Pimgdataunit);
var i : longint;
begin
  for i := 0 to 63 do temp[i] := p[zigzagpattern[i]];
  move(temp[0],p[0],64*sizeof(Timgdataunit));
end;

procedure zigzagify_block(p,temp : Pimgdataunit);
var i : longint;
begin
  for i := 0 to 63 do temp[zigzagpattern[i]] := p[i];
  move(temp[0],p[0],64*sizeof(Timgdataunit));
end;

procedure Tjpeg_imagecomponent.zigzagify();
var temparr : array[0..63] of Timgdataunit; bc,i : longint;
begin
  ensure_org(imgorg_DCT);
  bc := blockcount-1;
  for i := 0 to bc do zigzagify_block(@imgdat[i*64],@temparr[0]);
  imgorg := imgorg_DCT_ZZ;
end;

procedure Tjpeg_imagecomponent.unzigzagify();
var temparr : array[0..63] of Timgdataunit; bc,i : longint;
begin
  ensure_org(imgorg_DCT_ZZ);
  bc := blockcount-1;
  for i := 0 to bc do unzigzagify_block(@imgdat[i*64],@temparr[0]);
  imgorg := imgorg_DCT;
end;


procedure unblockify(p,ptemp : Pimgdataunit; blocksperrow : longint);
var bpr,dst,src,y,b : longint;
begin
  bpr := blocksperrow-1;
  dst := 0;
  for y := 0 to 7 do begin
    src := y*8;
    for b := 0 to bpr do begin
      move(p[src],ptemp[dst],8*sizeof(Timgdataunit));
      src := src + 64;
      dst := dst + 8;
    end;
  end;
end;

procedure blockify(p,ptemp : Pimgdataunit; blocksperrow : longint);
var bpr,dst,src,y,b : longint;
begin
  bpr := blocksperrow-1;
  src := 0;
  for y := 0 to 7 do begin
    dst := y*8;
    for b := 0 to bpr do begin
      move(p[src],ptemp[dst],8*sizeof(Timgdataunit));
      src := src + 8;
      dst := dst + 64;
    end;
  end;
end;

procedure Tjpeg_imagecomponent.ConvertDCTtoScanline();
begin
  ensure_org(imgorg_DCT);
  ConvertDCTScanline();
end;

procedure Tjpeg_imagecomponent.ConvertScanlineToDCT();
begin
  ensure_org(imgorg_scanlines);
  ConvertDCTScanline();
end;


procedure Tjpeg_imagecomponent.ConvertDCTScanline();
var ptemp,p : Pimgdataunit; al,am,bc : longint;
begin
  al := 64*blocksperrow;
  am := al*sizeof(Timgdataunit);
  getmem(ptemp,am);
  p := @imgdat[0];
  bc := 0;
  while (bc<blockcount) do begin
    if (imgorg=imgorg_scanlines) then blockify(p,ptemp,blocksperrow)
    else unblockify(p,ptemp,blocksperrow);
    move(ptemp[0],p[0],am);
    bc := bc + blocksperrow;
    p := @p[al];
  end;
  freemem(ptemp,am);
  if (imgorg=imgorg_scanlines) then imgorg := imgorg_DCT
  else imgorg := imgorg_scanlines;
end;



procedure Tjpeg_imagecomponent.recalc_area(x,y,H,V,mH,mV : longint; blocky : boolean; o : Timgdataorganization);
begin
  scanH := H;
  scanV := V;
  xres := (x*scanH) div mH;
  if (mH*xres<x*scanH) then inc(xres);
  yres := (y*scanV) div mV;
  if (mV*yres<y*scanV) then inc(yres);
  xmem := xres;
  ymem := yres;
  if blocky then begin
    while ((xmem and 7)>0) do xmem := xmem +1;
    while ((ymem and 7)>0) do ymem := ymem +1;
    while ((xmem mod (8*scanH))>0) do xmem := xmem +8;
    while ((ymem mod (8*scanV))>0) do ymem := ymem +8;
  end else begin
    while ((xmem mod scanH)>0) do xmem := xmem +1;
    while ((ymem mod scanV)>0) do ymem := ymem +1;
  end;
  noninterleaved_mcusH := xres div 8;
  if ((xres and 7)>0) then inc(  noninterleaved_mcusH);
  noninterleaved_mcusV := yres div 8;
  if ((yres and 7)>0) then inc(  noninterleaved_mcusV);
  blocksperrow := xmem DIV 8;
  blockrows    := ymem DIV 8;
  blockcount := blocksperrow*blockrows;
  memtotal := xmem*ymem*sizeof(Timgdataunit);
  lastvalidimgdataunit := xmem*ymem-1;
  blockyok := blocky;
  imgorg := o;
end;

procedure Tjpeg_imagecomponent.makeup_area(p : pointer; keepondestroy : boolean);
begin
  if (memallocated>0) then begin
    if keeponkill then raise TjpegimageExcpt.create('forbidden img-area overwrite.');
    freemem(imgdat,memallocated);
    memallocated := 0;
    imgdat := nil;
  end;
  if (p=nil) then begin
    getmem(imgdat,memtotal);
    memallocated := memtotal;
  end else begin
    imgdat := p;
    memallocated := 0;
  end;
  keeponkill := keepondestroy;
  fillbyte(imgdat[0],memtotal,0);
end;

procedure Tjpeg_imagecomponent.take_qtab(p : Pquantitab);
begin
  if not((p=nil)or(has_quantitab)) then begin
    move(p^[0],quantitab[0],sizeof(Tquantitab));
    has_quantitab := true;
  end;
end;


constructor Tjpeg_imagecomponent.create();
begin
  inherited create();
  imgdat := nil;
  imgorg := imgorg_unspecified;
  xres := 0;
  yres := 0;
  blockcount := 0;
  lastvalidimgdataunit := 0;
  keeponkill := false;
  memallocated := 0;
  blocksperrow := 0;
  blockrows    := 0;
  blockcount := 0;
  memtotal := 0;
  lastvalidimgdataunit := -1;
  blockyok := false;
  has_quantitab := false;
  jpeg_rsti := 0;
  jpeg_pt := 0;
end;

destructor Tjpeg_imagecomponent.destroy();
begin
  if (memallocated>0) then begin
    if not(keeponkill) then begin
      freemem(imgdat,memallocated);
      imgdat := nil;
    end;
  end;
  inherited destroy();
end;


{$include jpeg_dct.pas}







procedure Tjpeg_imagecomponent.doINTidct();
var tmp1,tmp2,tmpq : plongint;
    p2 : Pimgdataunit; pq,pa : Plongint;
    i,j,k : longint; 
begin
  if not(jpeg_precision=8) then begin
    doidct();
    exit;
  end;
  if not(has_quantitab) then raise TjpegimageExcpt.create('missing quantization table.');
  ensure_org(imgorg_DCT_ZZ);
  unzigzagify();
  getmem(tmp1,64*sizeof(longint));
  getmem(tmp2,64*sizeof(longint));
  getmem(tmpq,64*sizeof(longint));
  brightwhite := $FFFF;
  for j := 0 to 63 do tmpq[j] := quantitab[j];
  i := 0; 
  k := lastvalidimgdataunit-63;
  while(i<=k) do begin
    pq := tmpq;
    p2 := @imgdat[i];
    for j := 0 to 63 do begin
      tmp1[j] := smallint(p2^)*pq^;
      inc(p2);
      inc(pq);
    end;
    INTidct(tmp1,tmp2);
    pa := tmp1;
    for j := 0 to 63 do begin
      if (abs(pa^)>=511) then begin
        if (pa^<0) then pa^ := -512
        else pa^ := 511;
      end;
      inc(pa);
    end;
    pa := tmp1;
    p2 := @imgdat[i];
    for j := 0 to 63 do begin
      p2^ := (pa^ shl 6) + 32768;
      inc(pa);
      inc(p2);
    end;
    i := i + 64;
  end;
  freemem(tmp1,64*sizeof(longint));
  freemem(tmp2,64*sizeof(longint));
  freemem(tmpq,64*sizeof(longint));
end;



procedure Tjpeg_imagecomponent.doidct();
var tmp1,tmp2,tmpq : Tfloatarr;
    poffs,poffs1,poffs0,poffsf : single;
    p2 : Pimgdataunit;
    i,j,k,precision : longint; 
begin
  if not(has_quantitab) then raise TjpegimageExcpt.create('missing quantization table.');
  ensure_org(imgorg_DCT_ZZ);
  unzigzagify();
  brightwhite := $FFFF;
  precision := max(min(jpeg_precision,16),1);
  poffs := (1 shl precision)-1;
  poffs1 := 65535;
  poffs0 := 0.0;
//  poffsf := (1 shl (16-precision));
//  poffs  := poffs*poffsf;
  poffsf := 65535 / poffs;
  poffs  := 65536;
  for j := 0 to 63 do tmpq[j] := quantitab[j]*0.25*poffsf;
  i := 0; 
  k := lastvalidimgdataunit-63;
  while(i<=k) do begin
    p2 := @imgdat[i];
    for j := 0 to 63 do begin
      tmp1[j] := smallint(p2^)*tmpq[j];
      inc(p2);
    end;
    tmp1[0] := tmp1[0]+poffs;
    idct(tmp1,tmp2);
    for j := 0 to 63 do tmp1[j] := max(min(tmp1[j],poffs1),poffs0);
    p2 := @imgdat[i];
    for j := 0 to 63 do begin
      p2^ := round(tmp1[j]);
      inc(p2);
    end;
    i := i + 64;
  end;
end;




procedure Tjpeg_imagecomponent.dofdct();
var tmp1,tmp2,tmpq : Tfloatarr;
    poffs,bw2 : single;
    p2 : Pimgdataunit;
    i,j,k,precision : longint; 
begin
  if not(has_quantitab) then raise TjpegimageExcpt.create('missing quantization table.');
  ensure_org(imgorg_DCT);
  precision := max(min(jpeg_precision,16),1);
  bw2 := brightwhite*0.5;
  poffs := (1 shl precision) / brightwhite;
  for j := 0 to 63 do tmpq[j] := quantitab[j];
  for j := 0 to 63 do tmpq[j] := poffs*0.25/tmpq[j];
  i := 0; 
  k := lastvalidimgdataunit-63;
  while(i<=k) do begin
    for j := 0 to 63 do tmp1[j] := (imgdat[i+j]-bw2);
    fdct(tmp1,tmp2);
    for j := 0 to 63 do tmp1[j] := tmp1[j]*tmpq[j];
    p2 := @imgdat[i];
    for j := 0 to 63 do begin
      p2^ := round(tmp1[j]);
      inc(p2);
    end;
    i := i + 64;
  end;
  zigzagify();
end;




{ For encoding, the coded image area might be a little
  larger than the specified user-res. In order to minimize
  the ecoded stream size and influence on the image quality
  (for dct)...
}
procedure Tjpeg_imagecomponent.fill_unused_imgarea(dct : boolean);
var x,y,xr,old : longint; p,po : Pimgdataunit;
begin
  ensure_org(imgorg_scanlines);
  xr := xmem-1;
  if (xres<xmem) then begin
    for y := 0 to yres-1 do begin
      if (dct and (xres>0)) then begin // use last valid color
        p := get_line_pointer(xres-1,y);
        old := p^;
        inc(p);
        for x := xres to xr do begin
          p^ := old;
          inc(p);
        end;
      end else begin // zero for lossles or empty
        p := get_line_pointer(xres,y);
        for x := xres to xr do begin
          p^ := 0;
          inc(p);
        end;
      end;
    end;
  end;
  for y := yres to ymem-1 do begin
    if (dct and (yres>0)) then begin // use last valid color
      p  := get_line_pointer(0,y);
      po := get_line_pointer(0,y-1);
      for x := xres to xr do begin
        p^ := po^;
        inc(p);
        inc(po);
      end;
    end else begin
      p := get_line_pointer(0,y);
      for x := 0 to xr do begin
        p^ := 0;
        inc(p);
      end;
    end;
  end;
end;



procedure Tjpeg_imagecomponent.do_undiff();
var rst,y,y2 : longint; p : Pimgdataunit;
begin
  rst := jpeg_rsti;
  if (rst=0) then rst := yres;
  y := 0;
  while (y<yres) do begin
    y2 := min(rst,yres-y);
    p := get_line_pointer(0,y);
    unDIFF(p,xmem,y2,jpeg_precision-jpeg_pt,jpeg_pred);
    y := y + y2;
  end;
  brightwhite := $FFFF shr (16-jpeg_precision+jpeg_pt);
//writeln('brightwhite=',hexstr(brightwhite,16));
end;

procedure Tjpeg_imagecomponent.do_diff17(psrc : Pimgdataunit; pr : longint);
var rst,y,y2 : longint; pdst : Pimgdataunit;
begin
  jpeg_pred := pr;
  rst := jpeg_rsti;
  if (rst=0) then rst := yres;
  pdst := get_line_pointer(0,0);
  y := 0;
  while (y<yres) do begin
    y2 := min(rst,yres-y);
    bdiff(@psrc[y*xmem],@pdst[y*xmem],xmem,y2,jpeg_precision-jpeg_pt,jpeg_pred);
    y := y + y2;
    inc(pdst,y2*xmem);
    inc(psrc,y2*xmem);
  end;
end;


{ For lossless mode writing:
  Calculate almost exactly how long the
  encoded comoponent will become, with the
  currently selected Predictor.  
}
function Tjpeg_imagecomponent.compute_lensum() : qword;
var x,y,ssss : longint; p : Pimgdataunit; q : qword; 
w : smallint; enc : Thuffenc; arr : array[0..16] of dword; 
begin
  for y := 0 to 16 do arr[y] := 0;
  for y := 0 to yres-1 do begin
    p := get_line_pointer(0,y);
    for x := xres-1 downto 0 do begin
      w := p^;
      if not(w=0) then begin
        ssss := bsrword(abs(w))+1;
      end else ssss := 0;
      inc(arr[ssss]);
      inc(p);
    end;
  end;
  q := 0;
  for y := 0 to 16 do q := q + y*qword(arr[y]); // size of raw data
  enc := Thuffenc.create();
  enc.set_symrange(16);
  for y := 0 to 16 do enc.inc_symbstat(y,arr[y]);
  enc.gencode(16);
  y := 0;
  for ssss := 0 to high(enc.bits) do begin // size of the huff-codes
    for x := enc.bits[ssss] downto 1 do begin
      q := q + ssss*qword( arr[enc.order[y]] );
      inc(y);
    end;
  end;
  enc.destroy();
  compute_lensum := q;
end;


procedure Tjpeg_imagecomponent.do_diff(pr : longint);
var a,besta : longint; x,bestx : qword; p : Pimgdataunit;
begin
  if (pr=0) then exit;
  if (pr>7) then raise TjpegimageExcpt.create('illegal LL prediction sheme.');
  getmem(p,memtotal);
  move(imgdat^,p^,memtotal);
  bestx := 0;
  besta := pr;
  if (pr<0) then begin
    for a := 1 to 7 do begin
      do_diff17(p,a);
      x := compute_lensum();
      if (x<bestx)or(a=1) then begin
        bestx := x;
        besta := a;
      end;
    end;
  end;
  do_diff17(p,besta);
  freemem(p,memtotal);
end;



procedure Tjpeg_imagecomponent.postprocess();
begin
  if (imgorg=imgorg_scanlines) then begin
    do_undiff(); 
  end else if(imgorg=imgorg_DCT_ZZ) then begin
    doidct();
//    doINTidct();
    ConvertDCTScanline();
  end;
end;


begin
  init_ktab();
  init_ftab();
end.

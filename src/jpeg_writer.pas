{$mode objfpc}
unit jpeg_writer;


interface
uses jpeg_imagecomponent,jpeg_sofmarkers,jpeg_headerdata,jpeg_streamwriter,jpeg_huffwriter,jpeg_huffenc,classes,sysutils, jpeg_misc;

type
Tjpegwriter = class(Tjpegstreamwriter)
  constructor create();
  destructor destroy(); override;



  procedure do_baseline(carr : Tcompoarr);
  procedure do_LL(carr : Tcompoarr);

  procedure do_soi();
  procedure do_eoi();
  procedure do_htab(a : Thuffenc; id : byte);
  procedure do_htabs(a,d : Thuffenc);
  procedure do_qtab(n : longint; c : Tjpeg_imagecomponent);
  procedure do_qtabs();
  procedure do_sof(h,p,x,y : longint);
  procedure do_sofll(h,p,x,y : longint; carr : Tcompoarr);
  procedure do_sos(ss,se,apl,aph : longint);
  procedure do_llsos(c,ss,se,apl,aph : longint);

  procedure check_compos_mcu_dct();
  procedure do_scan();

  public
  mcus_x,mcus_y : longint;

end;

type TjpegwriterExcpt = TjpegExcpt;





implementation
uses math;


procedure Tjpegwriter.check_compos_mcu_dct();
var i,mx,my : longint;
begin
  if (maxcompoinscan=0) then begin
    mcus_x := scancompo[0].noninterleaved_mcusH;
    mcus_y := scancompo[0].noninterleaved_mcusV;
  end else begin
    mcus_x := scancompo[0].blocksperrow div scancompo[0].scanH;
    mcus_y := scancompo[0].blockrows    div scancompo[0].scanV;
    for i := 1 to maxcompoinscan do begin
      mx := scancompo[i].blocksperrow div scancompo[i].scanH;
      my := scancompo[i].blockrows    div scancompo[i].scanV;
      if (mx<>mcus_x)or(my<>mcus_y) then raise TjpegwriterExcpt.create('Incompatible component dimensions');
    end;
  end;
end;

procedure Tjpegwriter.do_scan();
begin
  maxcompoinscan := high(scancompo);
  maxmcucol := mcus_x-1;
  maxmcurow := mcus_y-1;
  spectselstart := 0;
  spectselend   := 63;
  scan_DCT(0,0);
end;

procedure Tjpegwriter.do_LL(carr : Tcompoarr);
var acenc,dcenc : Thuffenc; i,x,y,p : longint;
begin
  x := 0;
  y := 0;
  p := 0;
  for i := 0 to high(carr) do begin
    x := max(carr[i].xres,x);
    y := max(carr[i].yres,y);
    p := max(carr[i].jpeg_precision,p);
  end;
  for i := 0 to high(carr) do begin
    carr[i].jpeg_precision := p; // precision must be shared by components
  end;
  do_soi();
  do_sofll($c3,p,x,y,carr);
  setlength(scancompo,1);
  maxcompoinscan := 0;
  for i := 0 to high(carr) do begin
    scancompo[0] := carr[i];
    carr[i].do_diff(-1);
//writeln('chosen predictor for ',i,'. compo: ',carr[i].jpeg_pred);
    dcenc := Thuffenc.create();
    dcenc.set_symrange(255);
    maxmcurow := carr[i].yres-1;
    maxmcucol := carr[i].xres-1;
    carr[i].wdctab := dcenc;
    testonly := true;
    scan_LL_single();
    testonly := false;
    dcenc.gencode(16);
    dcenc.generate_encoder();
    do_htab(dcenc,0);
    do_llsos(i,carr[i].jpeg_pred,0,0,0); //c,ss,se,apl,aph
    scan_LL_single();
    dcenc.destroy();
  end;
  do_eoi();
end;

procedure Tjpegwriter.do_baseline(carr : Tcompoarr);
var acenc,dcenc : Thuffenc; i : longint;
begin
  scancompo := carr;
  maxcompoinscan := high(scancompo);
  check_compos_mcu_dct();
  for i := 0 to maxcompoinscan do begin
    scancompo[i].jpeg_precision := 8;
  end;
  acenc := Thuffenc.create();
  dcenc := Thuffenc.create();
  acenc.set_symrange(255);
  dcenc.set_symrange(255);
  testonly := true;
  for i := 0 to maxcompoinscan do begin
    scancompo[i].wactab := acenc;
    scancompo[i].wdctab := dcenc;
  end;
  do_scan();
  testonly := false;
  acenc.gencode(16);
  dcenc.gencode(16);
  acenc.generate_encoder();
  dcenc.generate_encoder();
  do_soi();
  do_qtabs();
  do_sof($c0,8,scancompo[0].xres,scancompo[0].yres);
  do_htabs(acenc,dcenc);
  do_sos(0,63,0,0); 
  do_scan();
  do_eoi();
  acenc.destroy();
  dcenc.destroy();
end;



procedure Tjpegwriter.do_soi();
begin
  dst.writebyte($ff);
  dst.writebyte($d8);
end;

procedure Tjpegwriter.do_eoi();
begin
  dst.writebyte($ff);
  dst.writebyte($d9);
end;

procedure Tjpegwriter.do_htab(a : Thuffenc; id : byte);
var i,j,len : longint;
begin
  len := 17+2; j := 0;
  for i := 1 to high(a.bits) do begin
    j := j + a.bits[i];
  end;
  len := len + j;
  dst.writebyte($ff);
  dst.writebyte($c4);
  dst.writebyte(len shr 8);
  dst.writebyte(len and 255);
  dst.writebyte(id);
  for i := 1 to 16 do begin
    if (i<=high(a.bits)) 
    then dst.writebyte(a.bits[i])
    else dst.writebyte(0);
  end;
  dec(j);
  for i := 0 to j do dst.writebyte(a.order[i]);
end;

procedure Tjpegwriter.do_htabs(a,d : Thuffenc);
begin
  do_htab(d,0);
  do_htab(a,16);
end;

procedure Tjpegwriter.do_qtab(n : longint; c : Tjpeg_imagecomponent);
var i,j,len : longint; q,tmp : Tquantitab;
begin
  j := 0;
  for i := 0 to 63 do begin
    if c.quantitab[i]>255 then j := 1;
    q[i] := c.quantitab[i];
  end;
  zigzagify_block(@q[0],@tmp[0]);
  dst.writebyte($ff);
  dst.writebyte($db);
  len := 65+j*64 +2;
  dst.writebyte(len shr 8);
  dst.writebyte(len and 255);
  dst.writebyte(j*16+(n and 15));
  for i := 0 to 63 do begin
    if (j>0) then dst.writebyte(q[i] shr 8);
    dst.writebyte(q[i] and 255);
  end;
end;

procedure Tjpegwriter.do_qtabs();
var i : longint;
begin
  for i := 0 to maxcompoinscan do begin
    do_qtab(i,scancompo[i]);
  end;
end;


procedure Tjpegwriter.do_sof(h,p,x,y : longint);
var i,len : longint;
begin
  len := 8+3*(maxcompoinscan+1);
  dst.writebyte($ff);
  dst.writebyte(h);
  dst.writebyte(len shr 8);
  dst.writebyte(len and 255);
  dst.writebyte(p);
  dst.writebyte(y shr 8);
  dst.writebyte(y and 255);
  dst.writebyte(x shr 8);
  dst.writebyte(x and 255);
  dst.writebyte(maxcompoinscan+1);
  for i := 0 to maxcompoinscan do begin
    dst.writebyte(i);
    dst.writebyte(scancompo[i].scanH*16+(scancompo[i].scanV and 15));
    dst.writebyte(i);
  end;
end;

procedure Tjpegwriter.do_sofll(h,p,x,y : longint; carr : Tcompoarr);
var i,len : longint;
begin
  len := 8+3*(high(carr)+1);
  dst.writebyte($ff);
  dst.writebyte(h);
  dst.writebyte(len shr 8);
  dst.writebyte(len and 255);
  dst.writebyte(p);
  dst.writebyte(y shr 8);
  dst.writebyte(y and 255);
  dst.writebyte(x shr 8);
  dst.writebyte(x and 255);
  dst.writebyte(high(carr)+1);
  for i := 0 to high(carr) do begin
    dst.writebyte(i);
    dst.writebyte(carr[i].scanH*16+(carr[i].scanV and 15));
    dst.writebyte(0);
  end;
end;

procedure Tjpegwriter.do_sos(ss,se,apl,aph : longint);
var i,len : longint;
begin
  len := 6+2*(maxcompoinscan+1);
  dst.writebyte($ff);
  dst.writebyte($da);
  dst.writebyte(len shr 8);
  dst.writebyte(len and 255);

  dst.writebyte(maxcompoinscan+1);

  for i := 0 to maxcompoinscan do begin
    dst.writebyte(i);
    dst.writebyte(0); // dc/ac
  end;

  dst.writebyte(ss);
  dst.writebyte(se);
  dst.writebyte(aph*16+(apl and 15));

end;


procedure Tjpegwriter.do_llsos(c,ss,se,apl,aph : longint);
var i,len : longint;
begin
  len := 6+2*(0+1);
  dst.writebyte($ff);
  dst.writebyte($da);
  dst.writebyte(len shr 8);
  dst.writebyte(len and 255);

  dst.writebyte(0+1);

  dst.writebyte(c);
  dst.writebyte(0); // dc/ac

  dst.writebyte(ss);
  dst.writebyte(se);
  dst.writebyte(aph*16+(apl and 15));
end;


constructor Tjpegwriter.create();
begin
end;

destructor Tjpegwriter.destroy();
begin
  inherited destroy();
end;

begin
end.

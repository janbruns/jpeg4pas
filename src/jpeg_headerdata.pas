{$mode objfpc}
UNIT jpeg_headerdata;

INTERFACE
uses jpeg_sofmarkers,classes,sysutils, jpeg_misc;



PROCEDURE read_TSOFheader(src : Tstream; c : byte; VAR s : TSOFheader);
PROCEDURE read_scanheader(src : Tstream; VAR s : Tscanheader);



PROCEDURE print_SOFheader(CONST s : TSOFheader);
PROCEDURE print_SOSheader(CONST s : Tscanheader);

type TheaderreadExcpt = TjpegExcpt;

IMPLEMENTATION


PROCEDURE read_Tsofcomponent(src : Tstream; var c : TSOFcomponent);
BEGIN
  c.c := src.readbyte();
  c.V := src.readbyte();
  c.H := c.V shr 4;
  c.V := c.V and 15;
  c.Tq:= src.readByte();
END;

PROCEDURE read_TSOFheader(src : Tstream; c : byte; VAR s : TSOFheader);
VAR j,len : longint;
BEGIN
  with s do begin
    mode := c and 15;
    is_lossless := false;
    is_arithmetic := false;
    is_progressive := false;
    if (mode and 3)=3 then is_lossless := true;
    if (mode and 8)=8 then is_arithmetic := true;
    if (mode and 2)=2 then is_progressive := true;
    is_DCT := not(is_lossless);
  end;
  len := src.readByte()*256 + src.readByte();
  s.P := src.readByte();
  s.Y := src.readByte();
  s.Y := s.Y*256 + src.readByte();
  s.x := src.readByte();
  s.x := s.x*256 + src.readByte();
  s.Nf := src.readByte();
  if (len=8+3*s.Nf) then begin
    for j := 1 to s.Nf do begin
      read_Tsofcomponent(src,s.component[j]);
    end;
  end else raise TheaderreadExcpt.create('problem reading SOF header.');
END;



PROCEDURE read_scanheader(src : Tstream; VAR s : Tscanheader);
VAR j : longint;
BEGIN
  j := src.readByte()*256 + src.readByte();
  s.Ns := src.readByte();
  for j := 1 to s.Ns do begin
    s.component[j].Cs := src.readByte();
    s.component[j].Ta := src.readByte();
    s.component[j].Td := s.component[j].Ta shr 4;
    s.component[j].Ta := s.component[j].Ta and 15;
  end;
  s.Ss := src.readByte();
  s.Se := src.readByte();
  s.Al := src.readByte();
  s.Ah := s.Al shr 4;
  s.Al := s.Al and 15;
END;







PROCEDURE print_SOFheader(CONST s : TSOFheader);
VAR i : longint;
BEGIN
  with s do begin
    writeln('SOF',mode,' = ',SOFnames[mode]);
    writeln('  Sample precision                   : ',P);
    writeln('  Number of samples per line         : ',X);
    writeln('  Number of lines                    : ',Y);
    writeln('  Number of image components in frame: ',Nf);
    for i := 1 to Nf do begin
      writeln('    Component ',i:3,':');
      writeln('      Component identifier       : ',component[i].c);
      writeln('      Horizontal sampling factor : ',component[i].h);
      writeln('      Vertical sampling factor   : ',component[i].v);
      writeln('      Quantization table selector: ',component[i].Tq);
    end;
  end;
END;

PROCEDURE print_SOSheader(CONST s : Tscanheader);
VAR i : longint;
BEGIN
  with s do begin
    writeln('  Number of image components in scan        : ',Ns);
    writeln('  Start of spectral or predictor selection  : ',Ss);
    writeln('  End of spectral selection                 : ',Se);
    writeln('  Successive approximation bit position high: ',Ah);
    writeln('  Successive approximation bit position low : ',Al);
    for i := 1 to Ns do begin
      writeln('    Component ',i:3,':');
      writeln('      Scan component         : ',component[i].cs);
      writeln('      DC entropy coding table: ',component[i].Td);
      writeln('      AC entropy coding table: ',component[i].Ta);
    end;
  end;
END;




BEGIN
END.




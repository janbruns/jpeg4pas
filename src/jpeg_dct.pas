
{ .pas include file
  about some DCT implementations.
}

type Tfloatarr = array[0..63] of single;

{ reference forward 2D DCT implementation. 
}
procedure fdct_ref(var q1,q2 : Tfloatarr);
var sum,k1,k2,sq2 : real; x,y,x2,y2 : longint;
begin
  sq2 := 1/sqrt(2);
  for y := 0 to 7 do begin
    for x := 0 to 7 do begin
      sum := 0;
      for y2 := 0 to 7 do begin
        for x2 := 0 to 7 do begin
          k1 := cos((2*y2+1)*y*PI/16);
          k2 := cos((2*x2+1)*x*PI/16);
          sum := sum + k1*k2*q1[y2*8+x2];
        end;
      end;
      if (y=0) then k1 := sq2 else k1 := 1;
      if (x=0) then k2 := sq2 else k2 := 1;
     { leave out the factor /4, just because the other implementations 
       here also don't do that, moved to quantization table handling }
      q2[y*8+x] := k1*k2*sum;//*0.25; 
    end;
  end;
  move(q2[0],q1[0],sizeof(Tfloatarr));
end;


{ reference inverse 2D DCT implementation. 
}
procedure idct_ref(var q1,q2 : Tfloatarr);
var sum,k1,k2,sq2 : real; x,y,x2,y2 : longint;
begin
  sq2 := 1/sqrt(2);
  for y := 0 to 7 do begin
    for x := 0 to 7 do begin
      sum := 0;
      for y2 := 0 to 7 do begin
        for x2 := 0 to 7 do begin
          if (y2=0) then k1 := sq2 else k1 := 1;
          if (x2=0) then k2 := sq2 else k2 := 1;
          k1 := k1 * cos((2*y+1)*y2*PI/16);
          k2 := k2 * cos((2*x+1)*x2*PI/16);
          sum := sum + k1*k2*q1[y2*8+x2];
        end;
      end;
     { leave out the factor /4, just because the other implementations 
       here also don't do that, moved to quantization table handling }
      q2[y*8+x] := sum;//*0.25; 
    end;
  end;
  move(q2[0],q1[0],sizeof(Tfloatarr));
end;





{ Of course, it isn't very useful to recompute
  these very same cosines for any block... }

var k_tab : Tfloatarr;

procedure init_ktab();
var x,x2 : longint;
begin
  for x := 0 to 7 do begin
    for x2 := 0 to 7 do begin
      k_tab[x*8+x2] := cos((2*x+1)*x2*PI/16);
    end;
  end;
  for x := 0 to 7 do k_tab[x*8] := k_tab[x*8]/sqrt(2);
end;

procedure idct2(var q1,q2 : Tfloatarr);
var sum,k1,k2 : real; x,y,x2,y2 : longint;
begin
  for y := 0 to 7 do begin
    for x := 0 to 7 do begin
      sum := 0;
      for y2 := 0 to 7 do begin
        for x2 := 0 to 7 do begin
          k1 := k_tab[y*8+y2];
          k2 := k_tab[x*8+x2];
          sum := sum + k1*k2*q1[y2*8+x2];
        end;
      end;
      q2[y*8+x] := sum;
    end;
  end;
  move(q2[0],q1[0],sizeof(Tfloatarr));
end;


{ Of course, it isn't very useful to recompute
  these very same cosines for any block... }

var f_tab : Tfloatarr;

procedure init_ftab();
var x,x2 : longint;
begin
  for x := 0 to 7 do begin
    for x2 := 0 to 7 do begin
      f_tab[x*8+x2] := cos((2*x2+1)*x*PI/16);
    end;
  end;
end;

procedure fdct2(var q1,q2 : Tfloatarr);
var sum,k1,k2,sq2 : real; x,y,x2,y2 : longint;
begin
  sq2 := 1/sqrt(2);
  for y := 0 to 7 do begin
    for x := 0 to 7 do begin
      sum := 0;
      for y2 := 0 to 7 do begin
        for x2 := 0 to 7 do begin
          k1 := f_tab[y*8+y2];
          k2 := f_tab[x*8+x2];
          sum := sum + k1*k2*q1[y2*8+x2];
        end;
      end;
      if (y=0) then k1 := sq2 else k1 := 1;
      if (x=0) then k2 := sq2 else k2 := 1;
     { leave out the factor /4, just because the other implementations 
       here also don't do that, moved to quantization table handling }
      q2[y*8+x] := k1*k2*sum;//*0.25; 
    end;
  end;
  move(q2[0],q1[0],sizeof(Tfloatarr));
end;






{ the full 2D DCT of 8x8 samples
  can be split into 1D IDCTs of 8 samples,
  by first computing 8 horizontal IDCTs,
  then 8 IDCTs again for the other axis,
  with the results of the provious step as
  input.
}


procedure fdct(var q1,q2 : Tfloatarr);
var sum,k2,sq2 : real; x,y,x2,y2 : longint;
begin
  sq2 := 1/sqrt(2);
  for y := 0 to 7 do begin
    for x := 0 to 7 do begin
      sum := 0;
      for x2 := 0 to 7 do begin
        k2 := f_tab[x*8+x2];
        sum := sum + k2*q1[y*8+x2];
      end;
      if (x=0) then k2 := sq2 else k2 := 1;
      q2[y*8+x] := k2*sum;//*0.25; 
    end;
  end;
  for y := 0 to 7 do begin
    for x := 0 to 7 do begin
      sum := 0;
      for y2 := 0 to 7 do begin
        k2 := f_tab[y*8+y2];
        sum := sum + k2*q2[y2*8+x];
      end;
      if (y=0) then k2 := sq2 else k2 := 1;
      q1[y*8+x] := k2*sum;//*0.25; 
    end;
  end;
end;





procedure idct3(var q1,q2 : Tfloatarr);
var sum,k1 : real; x,y,x2,y2 : longint;
begin
  for y := 0 to 7 do begin
    for x := 0 to 7 do begin
      sum := 0;
      for x2 := 0 to 7 do begin
        k1 := k_tab[x*8+x2];
        sum := sum + k1*q1[y*8+x2];
      end;
      q2[y*8+x] := sum;
    end;
  end;
  for x := 0 to 7 do begin
    for y := 0 to 7 do begin
      sum := 0;
      for y2 := 0 to 7 do begin
        k1 := k_tab[y*8+y2];
        sum := sum + k1*q2[y2*8+x];
      end;
      q1[y*8+x] := sum;
    end;
  end;
end;


{ finally, manual loop unrolling for the 1D IDCT...
  saves probably one third of runtime compared to the
  previous version, but looks ugly.
}

type Pfloat = ^single;


{ktab:
 a  b  f  c  a  d  g  e
 a  c  g -e -a -b -f -d
 a  d -g -b -a  e  f  c
 a  e -f -d  a  c -g -b
 a -e -f  d  a -c -g  b
 a -d -g  b -a -e  f -c
 a -c  g  e -a  b -f  d
 a -b  f -c  a -d  g -e 

AA  := const_a*s[0] +const_a*s[4];
A_A := const_a*s[0] -const_a*s[4];
FG  := const_f*s[2] +const_g*s[6];
G_F := const_g*s[2] -const_f*s[6];
BCDE    := const_b*s[1] +const_c*s[3] +const_d*s[5] +const_e*s[7];
E_DC_B  := const_e*s[1] +const_d*s[3] +const_c*s[5] +const_b*s[7];
C_E_B_D := const_c*s[1] -const_e*s[3] -const_b*s[5] -const_d*s[7];
D_BEC   := const_d*s[1] -const_b*s[3] +const_e*s[5] +const_c*s[7];

0: a  b  f  c  a  d  g  e
7: a -b  f -c  a -d  g -e
3: a  e -f -d  a  c -g -b
4: a -e -f  d  a -c -g  b
AA FG BCDE E_DC_B

(AA)+(FG)+(BCDE)
(AA)+(FG)-(BCDE)
(AA)-(FG)+(E_DC_B)
(AA)-(FG)-(E_DC_B)


1: a  c  g -e -a -b -f -d
6: a -c  g  e -a  b -f  d
2: a  d -g -b -a  e  f  c
5: a -d -g  b -a -e  f -c
A_A G_F C_E_B_D D_BEC

(A_A)+(G_F)+(C_E_B_D)
(A_A)+(G_F)-(C_E_B_D)
(A_A)-(G_F)+(D_BEC)
(A_A)-(G_F)-(D_BEC)



}

procedure octoIDCT(s,r : Pfloat); // 1D
var AA,FG,BCDE,E_DC_B,A_A,G_F,C_E_B_D,D_BEC,tmp : real;
const
const_a : single = 7.071067691E-01;
const_b : single = 9.807852507E-01;
const_c : single = 8.314695954E-01;
const_d : single = 5.555702448E-01;
const_e : single = 1.950903237E-01;
const_f : single = 9.238795042E-01;
const_g : single = 3.826834261E-01;
begin
  AA := s^*const_a; 
  inc(s);
  A_A := AA;

  tmp := s^;
  inc(s);
  BCDE := tmp*const_b;
  E_DC_B := tmp*const_e;
  C_E_B_D := tmp*const_c;
  D_BEC := tmp*const_d;
  
  tmp := s^;
  inc(s);
  FG := tmp*const_f;
  G_F := tmp*const_g;

  tmp := s^;
  inc(s);
  BCDE += tmp*const_c;
  E_DC_B -= tmp*const_d;
  C_E_B_D -= tmp*const_e;
  D_BEC -= tmp*const_b;

  tmp := s^*const_a;
  inc(s);
  AA += tmp;
  A_A -= tmp;

  tmp := s^;
  inc(s);
  BCDE += tmp*const_d;
  E_DC_B += tmp*const_c;
  C_E_B_D -= tmp*const_b;
  D_BEC += tmp*const_e;

  tmp := s^;
  inc(s);
  FG += tmp*const_g;
  G_F -= tmp*const_f;

  tmp := s^;
  BCDE += tmp*const_e;
  E_DC_B -= tmp*const_b;
  C_E_B_D -= tmp*const_d;
  D_BEC += tmp*const_c;

  tmp := AA+FG;
  FG := AA-FG;
  AA := tmp;
  tmp := A_A+G_F;
  G_F := A_A-G_F;
  A_A := tmp;

  R^ := AA + BCDE;
  AA -= BCDE; //

  inc(r,8);
  R^ := A_A +C_E_B_D;
  inc(r,8);
  R^ := G_F +D_BEC;
  inc(r,8);
  R^ := FG + E_DC_B;
  inc(r,8);
  R^ := FG - E_DC_B;
  inc(r,8);
  R^ := G_F -D_BEC;
  inc(r,8);
  R^ := A_A -C_E_B_D;
  inc(r,8);
//  R^ := AA - BCDE;
  R^ := AA;
end;


procedure idct(var q1,q2 : Tfloatarr);
var x,y : longint;
begin
  for y := 0 to 7 do octoIDCT(@q1[y*8],@q2[y]);
  for x := 0 to 7 do octoIDCT(@q2[x*8],@q1[x]);
end;




procedure INToctoIDCT(s,r : Plongint); // 1D
var AA,FG,BCDE,E_DC_B,A_A,G_F,C_E_B_D,D_BEC,tmp : longint;
const
const_x = 16384;
const_a : longint = round(const_x*7.071067691E-01);
const_b : longint = round(const_x*9.807852507E-01);
const_c : longint = round(const_x*8.314695954E-01);
const_d : longint = round(const_x*5.555702448E-01);
const_e : longint = round(const_x*1.950903237E-01);
const_f : longint = round(const_x*9.238795042E-01);
const_g : longint = round(const_x*3.826834261E-01);
begin
  AA := s^*const_a; 
  inc(s);
  A_A := AA;

  tmp := s^;
  inc(s);
  BCDE := tmp*const_b;
  E_DC_B := tmp*const_e;
  C_E_B_D := tmp*const_c;
  D_BEC := tmp*const_d;
  
  tmp := s^;
  inc(s);
  FG := tmp*const_f;
  G_F := tmp*const_g;

  tmp := s^;
  inc(s);
  BCDE += tmp*const_c;
  E_DC_B -= tmp*const_d;
  C_E_B_D -= tmp*const_e;
  D_BEC -= tmp*const_b;

  tmp := s^*const_a;
  inc(s);
  AA += tmp;
  A_A -= tmp;

  tmp := s^;
  inc(s);
  BCDE += tmp*const_d;
  E_DC_B += tmp*const_c;
  C_E_B_D -= tmp*const_b;
  D_BEC += tmp*const_e;

  tmp := s^;
  inc(s);
  FG += tmp*const_g;
  G_F -= tmp*const_f;

  tmp := s^;
  BCDE += tmp*const_e;
  E_DC_B -= tmp*const_b;
  C_E_B_D -= tmp*const_d;
  D_BEC += tmp*const_c;

  tmp := AA+FG;
  FG := AA-FG;
  AA := tmp;
  tmp := A_A+G_F;
  G_F := A_A-G_F;
  A_A := tmp;

  R^ := SarLongint(AA  +BCDE,14);
  inc(r,8);
  R^ := SarLongint(A_A +C_E_B_D,14);
  inc(r,8);
  R^ := SarLongint(G_F +D_BEC,14);
  inc(r,8);
  R^ := SarLongint(FG  +E_DC_B,14);
  inc(r,8);
  R^ := SarLongint(FG  -E_DC_B,14);
  inc(r,8);
  R^ := SarLongint(G_F -D_BEC,14);
  inc(r,8);
  R^ := SarLongint(A_A -C_E_B_D,14);
  inc(r,8);
  R^ := SarLongint(AA  -BCDE,14);
end;

type Tlongintarr = array[0..63] of longint;

procedure INTidct(var q1,q2 : Plongint);
var x,y : longint;
begin
  for y := 0 to 7 do INToctoIDCT(@q1[y*8],@q2[y]);
  for x := 0 to 7 do INToctoIDCT(@q2[x*8],@q1[x]);
end;


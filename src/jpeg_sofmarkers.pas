
UNIT jpeg_sofmarkers;


INTERFACE



TYPE
TSOFcomponent = packed record
  C,H,V,Tq : byte;  
end;  


TSOFheader = record
  mode,P,y,X,Nf : longint;
  component : ARRAY[0..255] of TSOFcomponent;

  mcus_x,mcus_y : longint;
  is_arithmetic,
  is_DCT,
  is_progressive,
  is_lossless : boolean;
end;

Tscanheadercomponent = record
  Cs, Td, Ta : longint;
end;
Tscanheader = record
  Ns,Ss,Se,Ah,Al : longint;
  component : ARRAY[1..4] of Tscanheadercomponent;
end;





CONST
  SOFnames : ARRAY[0..15] of String = (
    'Baseline DCT, huffmann',
    'Extended sequential DCT, huffmann',
    'Progressive DCT, huffmann',
    'Lossless (sequential), huffmann',
    '-----',
    'Differential sequential DCT, huffmann',
    'Differential progressive DCT, huffmann',
    'Differential lossless (sequential), huffmann',
    'Reserved for JPEG extensions',
    'Extended sequential DCT, arithmetic',
    'Progressive DCT, arithmetic',
    'Lossless (sequential), arithmetic',
    '-------',
    'Differential sequential DCT, arithmetic',
    'Differential progressive DCT, arithmetic',
    'Differential lossless (sequential), arithmetic'
  );





IMPLEMENTATION




BEGIN
END.

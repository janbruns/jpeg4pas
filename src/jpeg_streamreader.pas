{$mode objfpc}
unit jpeg_streamreader;


interface
uses jpeg_imagecomponent,jpeg_huffreader,jpeg_huffdec,classes,sysutils, jpeg_misc;

type
Tjpegstreamreader = class(Thuffstreamreader)
  constructor create();
  destructor destroy(); override;

//  function readstuffedbyte() : byte; override;
  function handle_bytestuff(var b0 : byte) : boolean; virtual;

  { The following functions should take
    all the marker's data fields from the stream  }
  procedure handle_marker(m : byte); virtual;
  procedure handle_unknown_marker(m : byte); virtual;


  function read_LL_dataunit(hdec : Thuffdec) : word;
  procedure scan_LL_interl(sos_al,sos_ah : byte); 
  procedure scan_LL_single(sos_al,sos_ah : byte); 

  procedure scan_LL_dual_nosubsampling(); 
  procedure scan_LL_trip_nosubsampling(); 
  procedure scan_LL_quad_nosubsampling(); 


  procedure read_DCT_DC_to_pointer(p : Pimgdataunit; c : Tjpeg_imagecomponent);
  procedure scan_AC_DU(k : longint; p : Pimgdataunit; c : Tjpeg_imagecomponent);
  procedure scan_interl_compo_MCU(row,col : longint; c : Tjpeg_imagecomponent);
  procedure scan_single_compo_MCU(row,col : longint; c : Tjpeg_imagecomponent);
  procedure scan_DCT_MCU(row,col : longint); 
  procedure scan_DCT_baseline();


  function read_eobrun(n : longint) : longint;


  function succac_next_row() : boolean;
  function succac_next_blk(upd_eob : boolean) : boolean;
  function succac_next_k(upd_eob : boolean) : boolean;
  function succac_maybe_fetchbits() : boolean;
  function succac_zrl_overreadaction() : boolean;
  function succac_eob_overreadaction() : boolean;
  function succac_nz_overreadaction() : boolean;
  function succac_read_something() : boolean;
  function succac_do_something() : boolean;
  procedure succac_init();
  procedure DCT_succac();


  procedure scan_DCT(apprlow,apprhi : longint);


  procedure reset_all_lastDC();
  procedure startscan(); 

  procedure done_mcus(n : longint);
  procedure expect_restart();


  protected
  scancompo : array of Tjpeg_imagecomponent;
  scanstartstreampos : int64;
  maxcompoinscan : longint;
  maxmcurow,maxmcucol : longint;

  DCsuccbitcount,
  DCsuccscale : longint;
  spectselstart,
  spectselend     : longint;
  rstcount,rstintervall,scannedmcus : longint;

  disable_DC_pred : boolean;


  succac_blky : longint;

  private
  succac_dat : Pimgdataunit;
  succac_c  : Tjpeg_imagecomponent;
  succac_blkx,
  succac_k,
  succac_eob,
  succac_zrl : longint;
  succac_w : word;
  succac_wvalid,succac_is_subsequent,succac_waszero : boolean;
  
end;

type TjpegstreamreaderExcpt = TjpegExcpt;

FUNCTION jpeg_extend_val(v,t : word) : word; // Figure F.12 



implementation
const inital_lastDC = 0;


FUNCTION jpeg_extend_val(v,t : word) : word; inline; // Figure F.12 
BEGIN
  if (v<(1 shl (t-1))) then begin
    jpeg_extend_val := v + ($FFFF shl t) + 1;
  end else begin
    jpeg_extend_val := v;
  end;
END;

function Tjpegstreamreader.read_LL_dataunit(hdec : Thuffdec) : word; inline;
var ssss,w : word;
begin
  ssss := readHuffCodedValue(hdec);
  w := 0;
  if (ssss>=16) then w := $8000
  else if (ssss>0) then begin
    w := readDirectBits(ssss);
    w := jpeg_extend_val(w,ssss);
  end;
  read_LL_dataunit := w;
end;

procedure Tjpegstreamreader.scan_LL_dual_nosubsampling(); 
var row,col : longint; p1,p2 : Pimgdataunit; c1,c2 : Tjpeg_imagecomponent; tab1,tab2 : Thuffdec;
begin
  c1 := scancompo[0];
  c2 := scancompo[1];
  tab1 := c1.dctab;
  tab2 := c2.dctab;
  for row := 0 to maxmcurow do begin
    p1 := c1.get_line_pointer(0,row);
    p2 := c2.get_line_pointer(0,row);
    for col := 0 to maxmcucol do begin
      p1^ := read_LL_dataunit(tab1);
      inc(p1);
      p2^ := read_LL_dataunit(tab2);
      inc(p2);
    end;
    done_mcus(maxmcucol+1);
  end; 
end;

procedure Tjpegstreamreader.scan_LL_trip_nosubsampling(); 
var row,col : longint; p1,p2,p3 : Pimgdataunit; c1,c2,c3 : Tjpeg_imagecomponent; tab1,tab2,tab3 : Thuffdec;
begin
  c1 := scancompo[0];
  c2 := scancompo[1];
  c3 := scancompo[2];
  tab1 := c1.dctab;
  tab2 := c2.dctab;
  tab3 := c3.dctab;
  for row := 0 to maxmcurow do begin
    p1 := c1.get_line_pointer(0,row);
    p2 := c2.get_line_pointer(0,row);
    p3 := c3.get_line_pointer(0,row);
    for col := 0 to maxmcucol do begin
      p1^ := read_LL_dataunit(tab1);
      inc(p1);
      p2^ := read_LL_dataunit(tab2);
      inc(p2);
      p3^ := read_LL_dataunit(tab3);
      inc(p3);
    end;
    done_mcus(maxmcucol+1);
  end; 
end;

procedure Tjpegstreamreader.scan_LL_quad_nosubsampling(); 
var row,col : longint; p1,p2,p3,p4 : Pimgdataunit; c1,c2,c3,c4 : Tjpeg_imagecomponent; tab1,tab2,tab3,tab4 : Thuffdec;
begin
  c1 := scancompo[0];
  c2 := scancompo[1];
  c3 := scancompo[2];
  c4 := scancompo[3];
  tab1 := c1.dctab;
  tab2 := c2.dctab;
  tab3 := c3.dctab;
  tab4 := c4.dctab;
  for row := 0 to maxmcurow do begin
    p1 := c1.get_line_pointer(0,row);
    p2 := c2.get_line_pointer(0,row);
    p3 := c3.get_line_pointer(0,row);
    p4 := c4.get_line_pointer(0,row);
    for col := 0 to maxmcucol do begin
      p1^ := read_LL_dataunit(tab1);
      inc(p1);
      p2^ := read_LL_dataunit(tab2);
      inc(p2);
      p3^ := read_LL_dataunit(tab3);
      inc(p3);
      p4^ := read_LL_dataunit(tab4);
      inc(p4);
    end;
    done_mcus(maxmcucol+1);
  end; 
end;



procedure Tjpegstreamreader.scan_LL_interl(sos_al,sos_ah : byte); 
var row,col,x,y,c : longint; p : Pimgdataunit; nosubsampling : boolean;
begin
  startscan(); 
  nosubsampling := true;
  { unDIFF might need the restart-information.
    it expects to see: compo.rst = compo-line-count per restart-intervall.
    Tjpegstreamreader.rstintervall however is MCU-count per Intervall. }
  y := rstintervall div (maxmcucol+1); // mcu-rows per rstintervall
  for c := 0 to maxcompoinscan do begin
    with scancompo[c] do begin
      jpeg_rsti := scanV*y;
      jpeg_pt := sos_al;
      if (scanH<>1)or(scanV<>1) then nosubsampling := false;
    end;
  end;
  if nosubsampling then begin
    case maxcompoinscan of
      1 : scan_LL_dual_nosubsampling();
      2 : scan_LL_trip_nosubsampling();
      3 : scan_LL_quad_nosubsampling();
    end;
  end else begin
    for row := 0 to maxmcurow do begin
      for col := 0 to maxmcucol do begin
        for c := 0 to maxcompoinscan do with scancompo[c] do begin
          for y := 0 to scanv-1 do begin
            p := scancompo[c].get_line_pointer(col*scanH,row*scanV+y);
            for x := 0 to scanH-1 do begin
              p^ := read_LL_dataunit(dctab);
              inc(p);
            end;
          end;
        end;
      end;
      done_mcus(maxmcucol+1);
    end; 
  end;
  discardRemainingBits();
end;

procedure Tjpegstreamreader.scan_LL_single(sos_al,sos_ah : byte); 
var row,col : longint; p : Pimgdataunit; c : Tjpeg_imagecomponent;
begin
  startscan();
  c := scancompo[0];
  for row := 0 to maxmcurow do begin
    p := c.get_line_pointer(0,row);
    for col := 0 to maxmcucol do begin
      p^ := read_LL_dataunit(c.dctab);
      inc(p);
    end;
    done_mcus(maxmcucol+1);
  end;
  c.jpeg_rsti := rstintervall div (maxmcucol+1); // unDIFF might need that
  c.jpeg_pt := sos_al;
  discardRemainingBits();
end;



procedure Tjpegstreamreader.read_DCT_DC_to_pointer(p : Pimgdataunit; c : Tjpeg_imagecomponent);
var w : word;
begin
  if (disable_DC_pred) then c.lastDC := inital_lastDC;
  if (DCsuccbitcount=0) then begin
    w := read_LL_dataunit(c.dctab) + c.lastDC;
    c.lastDC := w;
    p^ := w;
  end else begin { subsequent approximation DC scan }
    p^ := readDirectBits(DCsuccbitcount) + (p^ * DCsuccscale);
  end;
end;

{
procedure Tjpegstreamreader.scan_AC_DU(k : longint; p : Pimgdataunit; c : Tjpeg_imagecomponent);
var zrl : longint; w : word; rs : byte; wvalid : boolean;
begin
  zrl := 0;
  wvalid := false;
  w := 0; 
  while (k<=spectselend) do begin
    if (zrl>0) then begin
      dec(zrl);
    end else begin
      if wvalid then begin
        p[k] := w;
        wvalid := false;
      end else begin
        rs := readHuffCodedValue(c.actab);
        if (rs=0) then break; // eob0
        if (rs=$F0) then begin
          zrl := 16;
          wvalid := false;
          dec(k);
        end else begin
          w := readDirectBits(rs and 15);
          w := jpeg_extend_val(w,rs and 15);
          zrl := ((rs shr 4) and 15);
          wvalid := true;
          dec(k);
        end;
      end;
    end;
    inc(k);
  end;
end;
}


procedure Tjpegstreamreader.scan_AC_DU(k : longint; p : Pimgdataunit; c : Tjpeg_imagecomponent);
var w,rs : longint; 
begin
  w := 0; 
  while (k<=spectselend) do begin
        rs := readHuffCodedValue(c.actab);
        if (rs=0) then break; // eob0
        if (rs=$F0) then begin
          k += 16;
        end else begin
          w := readDirectBits(rs and 15);
          w := jpeg_extend_val(w,rs and 15);
          k += ((rs shr 4) and 15);
          p[k] := w;
          inc(k);
        end;
  end;
end;


procedure Tjpegstreamreader.scan_interl_compo_MCU(row,col : longint; c : Tjpeg_imagecomponent);
var h,v,x,y,k : longint; p : Pimgdataunit; 
begin
  row := row * c.scanV;
  col := col * c.scanH;
  h := c.scanH-1;
  v := c.scanV-1;
  for y := 0 to v do begin
    for x := 0 to h do begin
      p := c.get_DCT_ZZ_block_pointer(col+x,row+y,0);
      k := spectselstart;
      if (k=0) then begin
        read_DCT_DC_to_pointer(p,c);
        k := k + 1;
      end;
      scan_AC_DU(k,p,c);
    end;
  end;
end;

procedure Tjpegstreamreader.scan_single_compo_MCU(row,col : longint; c : Tjpeg_imagecomponent);
var k : longint; p : Pimgdataunit; 
begin
  p := c.get_DCT_ZZ_block_pointer(col,row,0);
  k := spectselstart;
  if (k=0) then begin
    read_DCT_DC_to_pointer(p,c);
    k := k + 1;
  end;
  scan_AC_DU(k,p,c);
end;


procedure Tjpegstreamreader.scan_DCT_MCU(row,col : longint); 
var c : longint;
begin
  if (maxcompoinscan>0) then begin
    for c := 0 to maxcompoinscan do begin
      scan_interl_compo_MCU(row,col,scancompo[c]);
    end;
  end else begin
    scan_single_compo_MCU(row,col,scancompo[0]);
  end;
  done_mcus(1);
end;

procedure Tjpegstreamreader.scan_DCT_baseline(); 
var row,col : longint;
begin
  for row := 0 to maxmcurow do begin
    for col := 0 to maxmcucol do begin
      scan_DCT_MCU(row,col);
    end;
  end;
end;



{

  baseline, the version above ("all but succ ac"): 
  + spectral selection allowed
  + interleaved comopnents allowed
  - EOBRUN not allowed
  + subsequent DC approx allowed (multibit ready)


  succac, the version below
  - spectral selection needed
  - DC-coefficent not allowed (per spec)
  - interleaved comopnents not allowed
  + EOBRUN allowed
  + first+subsequent AC approx allowed

}

function Tjpegstreamreader.read_eobrun(n : longint) : longint;
begin
  if (n=0) then n := 1
  else n := readDirectBits(n and 15) or (1 shl n);
  read_eobrun := n;
end;



function Tjpegstreamreader.succac_next_row() : boolean;
begin
  inc(succac_blky);
  if (succac_blky>maxmcurow) then begin
    succac_next_row := false;
  end else begin
    succac_next_row := true;
  end;
end;

function Tjpegstreamreader.succac_next_blk(upd_eob : boolean) : boolean;
begin
  done_mcus(1);
  inc(succac_blkx);
  if (succac_blkx>maxmcucol) then begin
    succac_blkx := 0;
    succac_next_blk := false;
    if not(succac_next_row()) then exit;
  end;
  succac_k := spectselstart;
  succac_dat := succac_c.get_DCT_ZZ_block_pointer(succac_blkx,succac_blky,succac_k);
  if (upd_eob) then begin
    dec(succac_eob);
    if (succac_eob<0) then succac_eob := 0;
  end;
  succac_next_blk := true;
end;

function Tjpegstreamreader.succac_next_k(upd_eob : boolean) : boolean;
begin
  inc(succac_k);
  if (succac_k>spectselend) then begin
    succac_next_k := succac_next_blk(upd_eob);
  end else begin
    inc(succac_dat);
    succac_next_k := true;
  end;
end;

function Tjpegstreamreader.succac_maybe_fetchbits() : boolean;
var q : longint;
begin
  succac_maybe_fetchbits := false;
  if succac_is_subsequent and not(succac_dat^=0) then begin
    q := readDirectBits(DCsuccbitcount);
    succac_dat^ := DCsuccscale*succac_dat^ +q;
    succac_maybe_fetchbits := true;
  end;
end;

function Tjpegstreamreader.succac_eob_overreadaction() : boolean;
begin
  while (succac_eob>0) do begin
    succac_maybe_fetchbits();
    if not(succac_next_k(true)) then begin
      succac_eob_overreadaction := false;
      exit;
    end;
  end;
  succac_eob_overreadaction := true;
end;

function Tjpegstreamreader.succac_zrl_overreadaction() : boolean;
begin
  while (succac_zrl>0) do begin
    if not(succac_maybe_fetchbits()) then dec(succac_zrl);
    if not(succac_next_k(false)) then begin
      succac_zrl_overreadaction := false;
      exit;
    end;
  end;
  succac_zrl_overreadaction := true;
end;

function Tjpegstreamreader.succac_nz_overreadaction() : boolean;
begin
  succac_nz_overreadaction := true;
  while (succac_maybe_fetchbits()) do begin
    succac_nz_overreadaction := succac_next_k(true);
  end;
end;

function Tjpegstreamreader.succac_read_something() : boolean;
var rs : word;
begin
  rs := readHuffCodedValue(succac_c.actab);
  if (rs=$F0) then begin
    succac_zrl := 16;
    succac_read_something := succac_zrl_overreadaction();
  end else if ((rs and 15)=0) then begin
    succac_eob := read_eobrun(rs shr 4);
    succac_read_something := succac_eob_overreadaction();
  end else begin
    succac_w := readDirectBits(rs and 15);
    succac_w := jpeg_extend_val(succac_w,rs and 15);
    succac_zrl := ((rs shr 4) and 15);
    if (succac_zrl_overreadaction()) then begin
      if succac_nz_overreadaction() then begin
        succac_dat^ := succac_w
      end else begin
        raise TjpegstreamreaderExcpt.create('more mcu than expected.');
      end;
    end else begin
        raise TjpegstreamreaderExcpt.create('more mcus than expected.');
    end;
    succac_read_something := succac_next_k(false);
  end;
end;


function Tjpegstreamreader.succac_do_something() : boolean;
begin
  succac_do_something := succac_read_something();
end;

procedure Tjpegstreamreader.succac_init();
begin
  succac_eob := 0;
  succac_zrl := 0;
  succac_blkx := 0;
  succac_blky := 0;
  succac_k := spectselstart;
  succac_c := scancompo[0];
  succac_dat := succac_c.get_DCT_ZZ_block_pointer(succac_blkx,succac_blky,succac_k);
  succac_wvalid := false;
end;

procedure print_blockrow(n : longint; p : Pimgdataunit);
var i,j : longint;
begin
  for i := 0 to n do begin
    write(i:2,': ');
    for j := 0 to 63 do begin
      if (p^=0) then write('0') else write('x');
      inc(p);
    end;
    writeln;
  end;
end;

procedure Tjpegstreamreader.DCT_succac();
begin
  succac_init();

//  print_blockrow((maxmcucol),succac_c.get_DCT_ZZ_block_pointer(0,0,0) );

  while (succac_do_something()) do ;
//  writeln('remaining zrl:',succac_zrl,' eobrun:',succac_eob);
end;






procedure Tjpegstreamreader.scan_DCT(apprlow,apprhi : longint);
var withInterleave,withDC,withAC : boolean;
begin
  startscan(); 
  withdc := (spectselstart=0);
  withac := (spectselend>0)and(spectselstart<=spectselend);
  withInterleave := (maxcompoinscan>0);
  succac_is_subsequent := (apprhi>0);
  if (apprhi=0) then begin
    DCsuccbitcount := 0;
    DCsuccscale := 0;
    if (apprlow=0) then begin
      if not(withdc or withInterleave) then DCT_succac()
      else scan_DCT_baseline()
    end else begin
      if (withInterleave or withDC) then begin
        scan_DCT_baseline();
      end else if (withAC) then DCT_succac()
      else raise TjpegstreamreaderExcpt.create('error with spectral selection specs.');
    end;
  end else begin
    // subsequent successive mode
    DCsuccbitcount := apprhi-apprlow;    // normally=1
    DCsuccscale := 1 shl DCsuccbitcount; // normally=2
    if (withDC) then begin
      if (withAC) then raise TjpegstreamreaderExcpt.create('subsequent successive approximation scan with AC+DC not allowed');
      scan_DCT_baseline();
    end else begin
      if not(withAC) then raise  TjpegstreamreaderExcpt.create('error with spectral selection specs.');
      if (withInterleave) then raise TjpegstreamreaderExcpt.create('subsequent successive ac approximation with interleave');
      DCT_succac();
    end;
  end;
  discardRemainingBits();
end;



procedure Tjpegstreamreader.done_mcus(n : longint);
begin
  inc(rstcount,n);
  inc(scannedmcus,n);
  if (rstintervall>0) then begin
    if (rstcount=rstintervall) then begin
      expect_restart();
    end;
  end;
end;

procedure Tjpegstreamreader.expect_restart();
var b : byte;
begin
  if (scannedmcus<(maxmcurow+1)*(maxmcucol+1)) then begin
    b := readByte();
    if handle_bytestuff(b) then begin
      rstcount := 0;
      reset_all_lastDC();
      discardRemainingBits();
    end else begin
      raise TjpegstreamreaderExcpt.create('Bitstream-error: failed receiving RST-marker. @'+hexstr(position,8));
    end;
  end else begin
//    writeln('oh, no restart needed, scan complete.');
  end;
end;



procedure Tjpegstreamreader.reset_all_lastDC();
var i : longint;
begin
  for i := 0 to high(scancompo) do begin
    scancompo[i].lastDC := inital_lastDC;
  end;
end;

procedure Tjpegstreamreader.startscan(); 
begin
  scannedmcus := 0;
  rstcount := 0;
  succac_blky := 0;
  succac_waszero := false;
  scanstartstreampos := position;
  reset_all_lastDC();
  discardRemainingBits();
  maxcompoinscan := high(scancompo);
end;

procedure Tjpegstreamreader.handle_unknown_marker(m : byte);
var len : longint;
begin
  len := readByte() shl 8;
  len += readByte();
  if (len>=2) then begin
    seek(len-2,soFromCurrent);
  end else raise TjpegstreamreaderExcpt.create('Bitstream-error on handling marker.');
end;

procedure Tjpegstreamreader.handle_marker(m : byte);
begin
  handle_unknown_marker(m);
end;



function Tjpegstreamreader.handle_bytestuff(var b0 : byte) : boolean;
var b : byte;
begin
  handle_bytestuff := false;
  if (b0=255) then begin
    b := readByte();
    if not(b=0) then begin
      handle_marker(b);
      handle_bytestuff := true;
    end;
  end;
end;


constructor Tjpegstreamreader.create();
begin
  inherited create();
  DCsuccbitcount := 0;
  DCsuccscale := 0;
  disable_DC_pred := false;
  rstcount := 0;
  rstintervall := 0;
end;

destructor Tjpegstreamreader.destroy();
begin
  inherited destroy();
end;

begin
end.

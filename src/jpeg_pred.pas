
UNIT jpeg_pred;


INTERFACE

PROCEDURE unDIFF(p : pword; xres,yres,precision,predsel : longint);
PROCEDURE bdiff(psrc,pdst : pword;  xres,yres,precision,pr : longint);




IMPLEMENTATION

uses math;






PROCEDURE unDIFF1(p : pword; xres,yres,precision : longint);
VAR x,y,xr,i : longint; p2 : Pword;
BEGIN
  i := 1 shl (precision-1);
  for y := 0 to yres-1 do begin
    i += p[y*xres];
    p[y*xres] := i;
  end;
  xr := xres-1;
  for y := 0 to yres-1 do begin
    p2 := @p[y*xres];
    i := p2^;
    inc(p2);
    for x := 1 to xr do begin
      i += p2^;
      p2^ := i;
      inc(p2);
    end;
  end;
END;

PROCEDURE unDIFF2(p : pword; xres,yres,precision : longint);
VAR x,y,yr,i : longint; p2 : Pword;
BEGIN
  i := 1 shl (precision-1);
  for x := 0 to xres-1 do begin
    i += p[x];
    p[x] := i;
  end; 
  yr := yres-1;
  for x := 0 to xres-1 do begin
    p2 := @p[x];
    i := p2^;
    inc(p2,xres);
    for y := 1 to yr do begin
      i += p2^;
      p2^ := i;
      inc(p2,xres);
    end;
  end;
END;


PROCEDURE unDIFF_init3to7(p : pword; xres,yres,precision : longint);
VAR x,y,i : longint; 
BEGIN
  i := 1 shl (precision-1);
  for x := 0 to xres-1 do begin
    i += p[x];
    p[x] := i;
  end;
  i := p[0];
  for y := 1 to yres-1 do begin
    i += p[y*xres];
    p[y*xres] := i;
  end;
END;


PROCEDURE unDIFF_3to7(p : pword; xres,yres,precision,predsel : longint);
VAR x,y,a,b,c : longint; 
BEGIN
  unDIFF_init3to7(p,xres,yres,precision);
  for x := 1 to xres-1 do begin
    for y := 1 to yres-1 do begin
      a := p[y*xres + x -1];
      b := p[y*xres + x -xres];
      c := p[y*xres + x -xres -1];
      case predsel of
        3 : a := c;
        4 : a := +a +b -c;
        5 : a := a + ((b-c) shr 1); // we only need 16 of 32 bit , so shr is
        6 : a := b + ((a-c) shr 1); // the same as sar
        7 : a := (a+b) shr 1; // (a+b) is positive, so shr1 is the same as DIV2
      end;
      p[y*xres + x] += a;
    end;
  end;
END;

PROCEDURE unDIFF(p : pword; xres,yres,precision,predsel : longint);
BEGIN
//  write('UNDIFFING PREDICTOR:',predsel, ' at precision=',precision,' bit, xres=',xres,' , yres=',yres,' ...');
  case predsel of
    1 : unDIFF1(p,xres,yres,precision);
    2 : unDIFF2(p,xres,yres,precision);
    3,4,5,6,
    7 : unDIFF_3to7(p,xres,yres,precision,predsel);
  end;
//  writeln('done.');
END;




PROCEDURE bdiff1(psrc,pdst : pword;  xres,yres : longint);
VAR x,y,xr1,yr1 : longint; pd,p0,pa : Pword;
BEGIN
  yr1 := yres-1;
  xr1 := xres-1;
  for y := 1 to yr1 do begin
    pd := @pdst[y*xres];
    p0 := @psrc[y*xres];
    pd^ := p0^ -psrc[y*xres-xres];
    inc(pd);
    inc(p0);
    pa := @psrc[y*xres];    
    for x := 1 to xr1 do begin
      pd^ := p0^ - pa^;
      inc(pd);
      inc(p0);
      inc(pa);
    end;
  end;
END;


PROCEDURE bdiff2(psrc,pdst : pword;  xres,yres : longint);
VAR x,y,xr1,yr1 : longint; pd,p0,pb : Pword;
BEGIN
  yr1 := yres-1;
  xr1 := xres-1;
  for y := 1 to yr1 do begin
    pd := @pdst[y*xres];
    p0 := @psrc[y*xres];
    pd^ := p0^ -psrc[y*xres-xres];
    inc(pd);
    inc(p0);
    pb := @psrc[y*xres-xres+1];
    for x := 1 to xr1 do begin
      pd^ := p0^ - pb^;
      inc(pd);
      inc(p0);
      inc(pb);
    end;
  end;
END;

PROCEDURE bdiff3(psrc,pdst : pword;  xres,yres : longint);
VAR x,y,xr1,yr1 : longint; pd,p0,pc : Pword;
BEGIN
  yr1 := yres-1;
  xr1 := xres-1;
  for y := 1 to yr1 do begin
    pd := @pdst[y*xres];
    p0 := @psrc[y*xres];
    pd^ := p0^ -psrc[y*xres-xres];
    inc(pd);
    inc(p0);
    pc := @psrc[y*xres-xres];
    for x := 1 to xr1 do begin
      pd^ := p0^ - pc^;
      inc(pd);
      inc(p0);
      inc(pc);
    end;
  end;
END;

PROCEDURE bdiff4(psrc,pdst : pword;  xres,yres : longint);
VAR x,y,xr1,yr1 : longint; pd,p0,pa,pb,pc : Pword;
BEGIN
  yr1 := yres-1;
  xr1 := xres-1;
  for y := 1 to yr1 do begin
    pd := @pdst[y*xres];
    p0 := @psrc[y*xres];
    pd^ := p0^ -psrc[y*xres-xres];
    inc(pd);
    inc(p0);
    pa := @psrc[y*xres];
    pb := @psrc[y*xres-xres+1];
    pc := @psrc[y*xres-xres];
    for x := 1 to xr1 do begin
      pd^ := p0^ -(+pa^ +pb^ -pc^);
      inc(pd);
      inc(p0);
      inc(pa);
      inc(pb);
      inc(pc);
    end;
  end;
END;

PROCEDURE bdiff5(psrc,pdst : pword;  xres,yres : longint);
VAR x,y,xr1,yr1 : longint; pd,p0,pa,pb,pc : Pword;
BEGIN
  yr1 := yres-1;
  xr1 := xres-1;
  for y := 1 to yr1 do begin
    pd := @pdst[y*xres];
    p0 := @psrc[y*xres];
    pd^ := p0^ -psrc[y*xres-xres];
    inc(pd);
    inc(p0);
    pa := @psrc[y*xres];
    pb := @psrc[y*xres-xres+1];
    pc := @psrc[y*xres-xres];
    for x := 1 to xr1 do begin
      pd^ := p0^ -(   pa^ + ((longint(pb^)-longint(pc^)) shr 1)  );
      inc(pd);
      inc(p0);
      inc(pa);
      inc(pb);
      inc(pc);
    end;
  end;
END;

PROCEDURE bdiff6(psrc,pdst : pword;  xres,yres : longint);
VAR x,y,xr1,yr1 : longint; pd,p0,pa,pb,pc : Pword;
BEGIN
  yr1 := yres-1;
  xr1 := xres-1;
  for y := 1 to yr1 do begin
    pd := @pdst[y*xres];
    p0 := @psrc[y*xres];
    pd^ := p0^ -psrc[y*xres-xres];
    inc(pd);
    inc(p0);
    pa := @psrc[y*xres];
    pb := @psrc[y*xres-xres+1];
    pc := @psrc[y*xres-xres];
    for x := 1 to xr1 do begin
      pd^ := p0^ -(   pb^ + ((longint(pa^)-longint(pc^)) shr 1)  );
      inc(pd);
      inc(p0);
      inc(pa);
      inc(pb);
      inc(pc);
    end;
  end;
END;

PROCEDURE bdiff7(psrc,pdst : pword;  xres,yres : longint);
VAR x,y,xr1,yr1 : longint; pd,p0,pa,pb : Pword;
BEGIN
  yr1 := yres-1;
  xr1 := xres-1;
  for y := 1 to yr1 do begin
    pd := @pdst[y*xres];
    p0 := @psrc[y*xres];
    pd^ := p0^ -psrc[y*xres-xres];
    inc(pd);
    inc(p0);
    pa := @psrc[y*xres];
    pb := @psrc[y*xres-xres+1];
    for x := 1 to xr1 do begin
      pd^ := p0^ -(   (longint(pa^)+longint(pb^)) shr 1   );
      inc(pd);
      inc(p0);
      inc(pa);
      inc(pb);
    end;
  end;
END;

PROCEDURE bdiff_firstline(psrc,pdst : pword;  xres,yres,precision : longint);
VAR x,xr1 : longint;  pd,p0,pa : Pword;
BEGIN
  x := 1 shl (precision-1);
  pdst^ := psrc^ -x;
  xr1 := xres-1;
  pd := @pdst[1];
  p0 := @psrc[1];
  pa := @psrc[0];
  for x := 1 to xr1 do begin
    pd^ := p0^ -pa^;
    inc(pd);
    inc(p0);
    inc(pa);
  end;
END;

PROCEDURE bdiff(psrc,pdst : pword;  xres,yres,precision,pr : longint);
BEGIN
  bdiff_firstline(psrc,pdst,xres,yres,precision);
  case pr of
    1 : bdiff1(psrc,pdst,xres,yres);
    2 : bdiff2(psrc,pdst,xres,yres);
    3 : bdiff3(psrc,pdst,xres,yres);
    4 : bdiff4(psrc,pdst,xres,yres);
    5 : bdiff5(psrc,pdst,xres,yres);
    6 : bdiff6(psrc,pdst,xres,yres);
    7 : bdiff7(psrc,pdst,xres,yres);
  end;
END;



BEGIN
END.

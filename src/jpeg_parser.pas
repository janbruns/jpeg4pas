{$mode objfpc}
unit jpeg_parser;


interface
uses jpeg_imagecomponent,jpeg_sofmarkers,jpeg_headerdata,jpeg_streamreader,jpeg_streamwriter,jpeg_huffreader,jpeg_huffdec,classes,sysutils,jpeg_pred, jpeg_misc;

type
Tjpegparser = class(Tjpegstreamreader)
  constructor create();
  destructor destroy(); override;


  procedure parse(s : Tstream); virtual;

  procedure handle_marker(m : byte); override;

  procedure handle_APP(c : byte); virtual;
  procedure handle_SOI(c : byte); virtual;
  procedure handle_EOI(c : byte); virtual;
  procedure handle_COM(c : byte); virtual;


  procedure handle_DQT(c : byte); virtual;
  procedure handle_DNL(c : byte); virtual;
  procedure handle_DRI(c : byte); virtual;
  procedure handle_DHP(c : byte); virtual;
  procedure handle_DAC(c : byte); virtual;
  procedure handle_EXP(c : byte); virtual;
  procedure handle_TEM(c : byte); virtual;
  procedure handle_RST(c : byte); virtual;
  procedure handle_DHT(c : byte); virtual;
  procedure handle_SOS(c : byte); virtual;
  procedure handle_SOF(c : byte); virtual;

  function create_compo() : Tjpeg_imagecomponent; virtual;
  procedure get_all_components(var carr : Tcompoarr);
  procedure getres(var x,y : longint); 

  function readlen() : longint;

  protected
  function read_quantitab() : longint;
  function read_hufftab() : longint;
  procedure generate_components();
  procedure maybe_fix_missing_quantitabs();
  procedure do_scan();
  procedure lookup_components_for_scan();


  protected
  framecomponents : array[0..255] of Tjpeg_imagecomponent;
  quantitabs : array[0..15] of Pquantitab;
  hufftabs : array[0..31] of Thuffdec;

  scancount : longint;

  sofh : TSOFheader;
  sh : Tscanheader;
  do_pred,
  is_in_scan,done,seen_SOF : boolean;
end;

type TjpegparserExcpt = TjpegExcpt;




implementation




procedure Tjpegparser.parse(s : Tstream);
var cnt : longint; b : byte;
begin
  done := false;
  setStream(s);
  cnt := 0;
  while (read(b,1)=1) do begin
    if not(is_in_scan) then begin
      inc(cnt);
      if not(b=255) then begin
        raise TjpegparserExcpt.create('unexpected byte.');
      end else  handle_marker(readbyte());
    end else if (b=255) then handle_marker(readbyte());
    if done then break;
  end;
end;

function Tjpegparser.create_compo() : Tjpeg_imagecomponent;
begin
  create_compo := Tjpeg_imagecomponent.create();
end;



procedure Tjpegparser.handle_APP(c : byte);
begin
//  writeln('APP',c and 15);
  handle_unknown_marker(c);
end;


function Tjpegparser.read_quantitab() : longint;
var temparr : array[0..63] of Timgdataunit; i,x,pq,tq,v : longint; p : Pquantitab;
begin
  x := readbyte();
  pq := (x shr 4) and 1;
  tq := x and 15;
  new(p);
  if not(quantitabs[tq]=nil) then dispose(quantitabs[tq]);
  quantitabs[tq] := p;
  for i := 0 to 63 do begin
    v := readbyte();
    if (pq>0) then v := v*256 + readbyte();
    p^[i] := v;
  end;
  unzigzagify_block(@p^[0],temparr);
  read_quantitab := 65+64*Pq;
end;

procedure Tjpegparser.handle_DQT(c : byte);
var len : longint;
begin
  len := readlen();
  while (len>0) do len := len-read_quantitab();
  if (len<0) then raise TjpegparserExcpt.create('problem defining quantitabs.');
end;

procedure Tjpegparser.handle_TEM(c : byte);
begin
end;






procedure Tjpegparser.handle_COM(c : byte);
var len : longint; s : ansistring;
begin
//  write('COM:');
  len := readlen();
  if (len>0) then begin
    setlength(s,len);
    readbuffer(s[1],len);
//    writeln(s,'#');
  end;
end;



function Tjpegparser.read_hufftab() : longint;
var bl : Tbitlen; v : Tdatarr; x,i,sum : longint; 
begin
  x := readbyte();
//  writeln('reading hufftab ',x);
  x := x and 31;
  setlength(bl,17);
  bl[0] := 0;
  sum := 0;
  for i := 1 to 16 do begin
    bl[i] := readbyte();
    sum := sum + bl[i];
  end;
  setlength(v,sum);
  for i := 0 to sum-1 do v[i] := readbyte();
  read_hufftab := sum + 17;
  hufftabs[x].free();
  hufftabs[x] := Thuffdec.create();
  hufftabs[x].generate_decoder(bl,v);
end;


procedure Tjpegparser.handle_DHT(c : byte);
var len : longint;
begin
  len := readlen();
  while (len>0) do len := len-read_hufftab();
  if (len<0) then raise TjpegparserExcpt.create('problem defining hufftabs.');
end;





{ some image writers put quantization tables inside the frame,
  after already referencing them
}

procedure Tjpegparser.maybe_fix_missing_quantitabs();
var i : longint;
begin
  for i := 0 to maxcompoinscan do begin
    scancompo[i].take_qtab(quantitabs[scancompo[i].qtabid]);
    scancompo[i].jpeg_pred := sh.ss;
    scancompo[i].is_in_scan := false;
  end;
  for i := 0 to maxcompoinscan do begin
    if scancompo[i].is_in_scan  then raise TjpegparserExcpt.create('image component referenced multiple times in scan.');
    scancompo[i].is_in_scan := true;
  end;
end;

procedure Tjpegparser.lookup_components_for_scan();
var i,c : longint;
begin
  if (sh.Ns<=0) then raise TjpegparserExcpt.create('cant not scan no compnents not.');
  setlength(scancompo,sh.Ns);
  maxcompoinscan := high(scancompo);
  for i := 0 to sh.Ns-1 do begin
    c := sh.component[i+1].Cs;
    scancompo[i] := framecomponents[c];
    if (scancompo[i]=nil) then raise TjpegparserExcpt.create('unused image component in scan.');
    scancompo[i].actab := hufftabs[(sh.component[i+1].ta and 15) or 16];
    scancompo[i].dctab := hufftabs[(sh.component[i+1].td and 15)];
  end;
  maybe_fix_missing_quantitabs();
end;


procedure Tjpegparser.do_scan();
begin
  spectselstart := 0;
  spectselend   := 0;
  lookup_components_for_scan();
  if sofh.is_arithmetic then begin
    raise TjpegparserExcpt.create('arithmetic coded image. Unsupported.');
  end else begin
    if sofh.is_dct then begin
      spectselstart := sh.ss;
      spectselend   := sh.se;
      if (high(scancompo)=0) then begin
        maxmcucol := scancompo[0].noninterleaved_mcusH-1;
        maxmcurow := scancompo[0].noninterleaved_mcusV-1;
        scan_DCT(sh.al,sh.ah);
      end else begin
        maxmcucol := sofh.mcus_x-1;
        maxmcurow := sofh.mcus_y-1;
        scan_DCT(sh.al,sh.ah);
      end;
    end else begin
      if (high(scancompo)=0) then begin
        maxmcucol := scancompo[0].xres-1;
        maxmcurow := scancompo[0].yres-1;
        scan_LL_single(sh.al,sh.ah);
      end else begin
        maxmcucol := sofh.mcus_x-1;
        maxmcurow := sofh.mcus_y-1;
        scan_LL_interl(sh.al,sh.ah);
      end;
    end;
  end;
  discardRemainingBits();
end;

procedure Tjpegparser.handle_SOS(c : byte);
begin
  if not seen_SOF then raise TjpegparserExcpt.create('scan before sof.');
  is_in_scan := true;
  read_scanheader(self,sh);
//  print_SOSheader(sh);
  do_scan();
  is_in_scan := false;
end;






function ceildiv(x,a : longint)  : longint;
var q : longint;
begin
  q := x div a;
  if (q*a<x) then inc(q);
  ceildiv := q;
end;

procedure Tjpegparser.get_all_components(var carr : Tcompoarr);
var i : longint;
begin
  setlength(carr,sofh.Nf);
  for i := 1 to sofh.Nf do begin
    carr[i-1] := framecomponents[sofh.component[i].c];
  end;
end;

procedure Tjpegparser.generate_components();
var i,maxh,maxv,h,v,xres,yres,c,tq : longint; o : Timgdataorganization;
begin
  maxH := 1;
  maxV := 1;
  xres := sofh.x;
  yres := sofh.y;
  for i := 1 to sofh.Nf do begin
    h := sofh.component[i].h;
    v := sofh.component[i].v;
    if (h>maxh) then maxh := h;
    if (v>maxv) then maxv := v;
  end;
  for i := 1 to sofh.Nf do begin
    c := sofh.component[i].c;
    h := sofh.component[i].h;
    v := sofh.component[i].v;
    tq := sofh.component[i].tq;
    if (framecomponents[c]=nil) then begin
      o := imgorg_scanlines;
      if (sofh.is_dct) then o := imgorg_DCT_ZZ;
      framecomponents[c] := create_compo();
      with framecomponents[c] do begin
        recalc_area(sofh.x,sofh.y,h,v,maxH,maxV,true,o);
        makeup_area(nil,false);
        qtabid := tq;
        nam := c;
        jpeg_precision := sofh.P;
        if (i=1) then begin
          sofh.mcus_x := blocksperrow DIV scanH;
          sofh.mcus_y := blockrows    DIV scanV;
        end;
      end;
    end else raise TjpegparserExcpt.create('duplicate frame-component id.');
  end;
  if (sofh.is_lossless) then begin
    sofh.mcus_x := xres DIV maxH;
    if ((xres mod maxH)>0) then inc(sofh.mcus_x);
    sofh.mcus_y := yres DIV maxV;
    if ((yres mod maxV)>0) then inc(sofh.mcus_y);
  end;
end;



procedure Tjpegparser.handle_SOF(c : byte);
begin
  scancount := 0;
  read_TSOFheader(self,c,sofh);
//  print_SOFheader(sofh);
  generate_components();
  seen_SOF := true;
end;

procedure Tjpegparser.handle_EOI(c : byte);
begin
end;

procedure Tjpegparser.handle_SOI(c : byte);
begin
  rstintervall := 0;
end;

procedure Tjpegparser.handle_RST(c : byte);
begin
//  writeln('RST',c and 7,'inscan=',is_in_scan);
end;

procedure Tjpegparser.handle_DRI(c : byte);
var i : longint;
begin
  i := readlen();
  if (i=2) then begin
    i := readbyte();
    i := i*256+readbyte();
    rstintervall := i;
  end;
end;

procedure Tjpegparser.handle_marker(m : byte);
begin
  if (m=0) then exit;
//  writeln('Marker $',hexstr(m,2),' at streampos $',hexstr(position-1,8));
  if not(is_in_scan) then begin
    while (m=255) do m := readbyte();
    case m of
      $01 : handle_TEM(m);
      $c4 : handle_DHT(m);
      $c0 .. $c3,
      $c5 .. $cb,
      $cd .. $cf : handle_SOF(m);
      $cc : handle_DAC(m);
      $e0..$ef : handle_APP(m);
      $d0..$d7 : handle_RST(m);
      $d8 : handle_SOI(m);
      $d9 : handle_EOI(m);
      $da : handle_SOS(m);
      $db : handle_DQT(m);
      $dc : handle_DNL(m);
      $dd : handle_DRI(m);
      $de : handle_DHP(m);
      $df : handle_EXP(m);
      $fe : handle_COM(m);
      else if not(m=0) then begin
        raise TjpegparserExcpt.create('unknown marker $'+hexstr(m,2));
        handle_unknown_marker(m);
      end;
    end;
  end else begin // in scan
    case m of
      $d0..$d7 : handle_RST(m);
      else raise TjpegparserExcpt.create('disallowed marker $'+hexstr(m,2)+' inside scan '+inttostr(scancount)+'. blky='+inttostr(succac_blky));
    end;
  end;
end;









procedure Tjpegparser.handle_DNL(c : byte);
begin
  raise TjpegparserExcpt.create('DNL-markers currently unsupported.');
end;

procedure Tjpegparser.handle_DHP(c : byte);
begin
  raise TjpegparserExcpt.create('Hierarchical encoded images currently unsupported.');
end;

procedure Tjpegparser.handle_EXP(c : byte);
begin
  raise TjpegparserExcpt.create('Hierarchical encoded images currently unsupported.');
end;

procedure Tjpegparser.handle_DAC(c : byte);
begin
  raise TjpegparserExcpt.create('Arithmetic encoded images currently unsupported.');
end;

function Tjpegparser.readlen() : longint;
begin
  readlen := readbyte()*256 + readbyte() -2;
end;

procedure Tjpegparser.getres(var x,y : longint); 
begin
  x := sofh.x;
  y := sofh.y;
end;



constructor Tjpegparser.create();
var i : longint;
begin
  inherited create();
  for i := 0 to 255 do framecomponents[i] := nil;
  for i := 0 to 15 do quantitabs[i] := nil;
  for i := 0 to 31 do hufftabs[i] := nil;
  is_in_scan := false;
  seen_SOF := false;
end;

destructor Tjpegparser.destroy();
var i : longint;
begin
  for i := 0 to 255 do begin
    freeandnil(framecomponents[i]);
  end;
  for i := 0 to 15 do begin
    if not(quantitabs[i]=nil) then dispose(quantitabs[i]);
    quantitabs[i] := nil;
  end;
  for i := 0 to 31 do begin
    freeandnil(hufftabs[i]);
  end;
  inherited destroy();
end;

begin
end.

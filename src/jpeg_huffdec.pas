{$mode objfpc}
unit jpeg_huffdec;

interface
uses classes,sysutils,
     jpeg_misc; 

{ A Huffmann-decoder for use with relatively short 
  max-length codes.
}

type
Thuffdat = byte; // values to associate with code patterns
Tdatarr = array of Thuffdat;
Tbitlen = array of dword; // tells how many codes are with any code-length
Thuffdec = class

  public
  { Look for a huff-code w.
    w is assumed to be left (MSB) aligned,
    so that code-length grows to LSB.
    If there is a valid code in the MSBs of w,
    it should be deteced without masking out
    the trailing bits.
    The caller only has to verfiy, that the
    actual code length as returned by lookup()
    really was filled with that many valid 
    huff-stream bits.
  }
  function lookup(w : dword; var dat : Thuffdat) : dword;

  private
  { Instead of doing multiple lookups with masked out
    bits of the stream until a valid code is reached,
    we'll use a table with maximum code-length, and
    the valid codes entered on any position we would
    otherwise have to mask out on lookup.
  }
  procedure register_code(c,cl : dword; dat : Thuffdat);
  procedure setMaxCodelen(len : dword);


  public
  procedure generate_decoder(bitlen : Tbitlen; vals : Tdatarr);
  constructor create();
  destructor destroy(); override;

  private
  hufflen : array of byte;
  huffdat : Tdatarr;
  msb_code, // from bit31 to msb of codemask
  codemask, // mask for table lookups
  codebits : dword; // maximum codelength
end;

type Thuffdecexcpt = TjpegExcpt;

implementation


procedure Thuffdec.register_code(c,cl : dword; dat : Thuffdat);
var a,b,cnt : dword; 
begin
  a := (c shr msb_code) and codemask;
  cnt := codemask shr cl;
  for cnt := cnt downto 0 do begin
    b := a or cnt;
    if not(hufflen[b]=0) then raise Thuffdecexcpt.create('Code entry point already reserved.');
//writeln('set hufflen[',b,']=',cl,' huffdat[',b,']',dat,' cm=',codemask);
    hufflen[b] := cl;
    huffdat[b] := dat;
  end;
end;

procedure Thuffdec.setMaxCodelen(len : dword);
begin
  codebits := len;
  codemask := (1 shl codebits)-1;
  msb_code := 32-len;
  setlength(huffdat,codemask+1);
  setlength(hufflen,codemask+1);
  fillbyte(hufflen[0],codemask+1,0);
end;

procedure Thuffdec.generate_decoder(bitlen : Tbitlen; vals : Tdatarr);
var i,j,maxi,n : longint; c,rc : dword;
begin
  maxi := 0; n := -1;
  for i := 1 to high(bitlen) do begin
    if (bitlen[i]>0) then maxi := i;
    n := n + bitlen[i];
  end;
  if (n>high(vals)) then raise Thuffdecexcpt.create('More code-points ('+inttostr(n+1)+')than user-values('+inttostr(high(vals)+1)+') requested.');
  if (n<high(vals)) then raise Thuffdecexcpt.create('Less code-points ('+inttostr(n+1)+')than user-values('+inttostr(high(vals)+1)+') requested.');
  setMaxCodelen(maxi);
  c := 0; n := 0;
  for i := 1 to maxi do begin
    for j := bitlen[i] downto 1 do begin
//writeln(binstr(c,i),' : ',vals[n]);
      rc := c shl (32-i);
      register_code(rc,i,vals[n]);
      inc(c); inc(n);
    end;
    c := c shl 1;
  end;
end;

function Thuffdec.lookup(w : dword; var dat : Thuffdat) : dword;
var a : dword;
begin
  a := (w shr msb_code) and codemask;
  dat := huffdat[a];
  lookup := hufflen[a];
end;


constructor Thuffdec.create();
begin
  codebits := 0;
  codemask := 0;
  msb_code := 32;
  setlength(huffdat,0);
  setlength(hufflen,0);
end;

destructor Thuffdec.destroy();
begin
  codebits := 0;
  codemask := 0;
  msb_code := 32;
  setlength(huffdat,0);
  setlength(hufflen,0);
end;

begin
end.


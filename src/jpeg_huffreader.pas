{$mode objfpc}
unit jpeg_huffreader;


interface
uses jpeg_huffdec,classes,sysutils, jpeg_misc;

type
Thuffstreamreader = class(Tstream)
  function readDirectBits(n : dword) : dword;
  function readHuffCodedValue(hdec : Thuffdec) : Thuffdat;
  constructor create();
  destructor destroy(); override;

  procedure setStream(s : Tstream);
  function Read(var Buffer; Count: Longint): Longint; override; 
//  function Write(const Buffer; Count: Longint): Longint; override;

  function Seek(Offset: Longint; Origin: Word): Longint; override; 
 // function Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;


  function readabyte() : byte;

  function readstuffedbyte() : byte;

  function getbuffbitcnt() : byte;
  procedure discardRemainingBits;

  private
  procedure fetchbyte();
  procedure discardBits(n : dword);

  private
  bitdat : dword;
  bitsavail : dword;
  curpos,curlen : longint;
  dat : pbyte;
end;

type ThuffstreamreaderExcpt = TjpegExcpt;


implementation

function Thuffstreamreader.read(var Buffer; Count: Longint): Longint;
begin
  if (curpos+count>curlen) then count := curlen-curpos;
  if (count>0) then begin
    move(dat[curpos],buffer,count);
    inc(curpos,count);
  end;
  read := count;
end;



function Thuffstreamreader.Seek(Offset: Longint; Origin: Word): Longint;
begin
  if (origin=soFromBeginning) then begin
    curpos := Offset;
  end else if (origin=soFromCurrent) then begin
    inc(curpos,Offset);
  end else if (origin=soFromEnd) then begin
    curpos := curlen-Offset;
  end;
  if (curpos>curlen) then curpos := curlen;
  if (curpos<0) then curpos := 0;
  seek := curpos;
end;




procedure Thuffstreamreader.setStream(s : Tstream);
begin
  curlen := s.size;
  getmem(dat,curlen);
  s.readbuffer(dat[0],curlen);
  curpos := 0;
  s.seek(0,soFromBeginning);
end;


function Thuffstreamreader.readabyte() : byte;
var b : byte;
begin
  if (curpos<curlen) then begin
    b := dat[curpos];
    inc(curpos);
    readaByte := b;
  end else raise ThuffstreamreaderExcpt.create('unexpected stream end.');
end;

function Thuffstreamreader.readstuffedbyte() : byte;
var b : byte;
begin
  if (curpos<curlen) then begin
    b := dat[curpos];
    inc(curpos);
    if (b=255) then begin
      if (curpos<curlen) then begin
        b := dat[curpos];
        inc(curpos);
        if (b=0) then b := 255
        else raise ThuffstreamreaderExcpt.create('byte-unstuffing problem with bistream. Unexpected marker: $'+hexstr(b,2));
      end else raise ThuffstreamreaderExcpt.create('unexpected stream end.');
    end;
    readstuffedByte := b;
  end else raise ThuffstreamreaderExcpt.create('unexpected stream end.');
end;

procedure Thuffstreamreader.fetchByte();
var b : dword;
begin
  if (bitsavail<=24) then begin
    b := readstuffedByte();
    bitdat := bitdat or (b shl (24-bitsavail));
    bitsavail := bitsavail + 8;
  end else raise ThuffstreamreaderExcpt.create('Bitstream decoding error.');
end;

procedure Thuffstreamreader.discardBits(n : dword);
begin
  bitdat := bitdat shl n;
  bitsavail := bitsavail - n;
end;

function Thuffstreamreader.readDirectBits(n : dword) : dword;
begin
  while (bitsavail<n) do fetchbyte();
  readDirectBits := bitdat shr (32-n);
//  discardBits(n);
  bitdat := bitdat shl n;
  bitsavail := bitsavail - n;
end;

function Thuffstreamreader.readHuffCodedValue(hdec : Thuffdec) : Thuffdat;
var len : dword; v : Thuffdat;
begin
  repeat
    len := hdec.lookup(bitdat,v);
    if (len>0)and(len<=bitsavail) then begin
      //discardBits(len);
      bitdat := bitdat shl len;
      bitsavail := bitsavail - len;
      readHuffCodedValue := v;
      exit;
    end else begin
//      fetchbyte();
      if (bitsavail<=24) then begin
        len := readstuffedByte();
        bitdat := bitdat or (len shl (24-bitsavail));
        bitsavail := bitsavail + 8;
      end else raise ThuffstreamreaderExcpt.create('Bitstream decoding error.');
    end;
  until false;
end;

function Thuffstreamreader.getbuffbitcnt() : byte;
begin
  getbuffbitcnt := bitsavail;
end;

procedure Thuffstreamreader.discardRemainingBits();
begin
  bitdat := 0;
  bitsavail := 0;
end;


constructor Thuffstreamreader.create();
begin
  inherited create();
  bitdat := 0;
  bitsavail := 0;
  dat := nil;
end;

destructor Thuffstreamreader.destroy();
begin
  inherited destroy();
  if not(dat=nil) then freemem(dat,curlen);
  dat := nil;
  bitsavail := 0;
end;

begin
end.

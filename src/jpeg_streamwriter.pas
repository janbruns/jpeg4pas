{$mode objfpc}
unit jpeg_streamwriter;


interface
uses jpeg_imagecomponent,jpeg_huffwriter,jpeg_huffenc,classes,sysutils, jpeg_misc;

type


Tjpegstreamwriter = class(Thuffstreamwriter)

  procedure write_LL_dataunit(henc : Thuffenc; w : word);
  procedure scan_LL_single(); 
  procedure write_DCT_SEQ_AC_dataunit(henc : Thuffenc; r,w : word);
  procedure write_DCT_DC_from_pointer(p : Pimgdataunit; c : Tjpeg_imagecomponent);
  procedure write_interleaved_compo_MCU(row,col : longint; c : Tjpeg_imagecomponent);
  procedure write_DCT_interl_MCU(row,col : longint); 
  procedure write_DCT_baseline(); 

  procedure scan_DCT(apprlow,apprhi : longint);
  procedure done_mcus(n : longint);
  procedure do_restart();
  procedure reset_all_lastDC();
  procedure startscan(); 
  constructor create();
  destructor destroy(); override;


  protected
  scancompo : Tcompoarr;
  scanstartstreampos : int64;
  maxcompoinscan : longint;
  maxmcurow,maxmcucol : longint;

  DCsuccbitcount,
  DCsuccscale : longint;
  spectselstart,
  spectselend     : longint;
  rstcount,rstintervall,restartsdone,writtenmcus : longint;

  disable_DC_pred : boolean;



  
end;

type TjpegstreamwriterExcpt = TjpegExcpt;



implementation
const inital_lastDC = 0;


FUNCTION jpeg_wextend_val(v : word) : word; inline;
BEGIN
  jpeg_wextend_val := v - (v shr 15);
END;

FUNCTION get_ssss(v : smallint) : longint; inline;
BEGIN
  if not(v=0) then begin
    get_ssss := bsrword(abs(v))+1;
  end else get_ssss := 0;
END;


procedure Tjpegstreamwriter.write_LL_dataunit(henc : Thuffenc; w : word);
var ssss : word;
begin
  ssss := get_ssss(w);
  writeHuffCodedValue(henc,ssss);
  if (ssss>0)and(ssss<16) then begin
    w := jpeg_wextend_val(w);
    writeDirectBits(w,ssss);
  end;
end;


procedure Tjpegstreamwriter.scan_LL_single(); 
var row,col : longint; p1 : Pimgdataunit; c1 : Tjpeg_imagecomponent; tab1 : Thuffenc;
begin
  startscan(); 
  c1 := scancompo[0];
  tab1 := c1.wdctab;
  for row := 0 to maxmcurow do begin
    p1 := c1.get_line_pointer(0,row);
    for col := 0 to maxmcucol do begin
      write_LL_dataunit(tab1,p1^);
      inc(p1);
    end;
    done_mcus(maxmcucol+1);
  end;
  pad(1);
end;

procedure Tjpegstreamwriter.write_DCT_SEQ_AC_dataunit(henc : Thuffenc; r,w : word);
var ssss : word;
begin
  ssss := get_ssss(w);
  writeHuffCodedValue(henc,ssss or (r*16));
  w := jpeg_wextend_val(w);
  writeDirectBits(w,ssss);
end;


procedure Tjpegstreamwriter.write_DCT_DC_from_pointer(p : Pimgdataunit; c : Tjpeg_imagecomponent);
var w : word;
begin
  if (disable_DC_pred) then c.lastDC := inital_lastDC;
  if (DCsuccbitcount=0) then begin
    w := p^;
    w := w -c.lastDC;
    c.lastDC := p^;
    w := SarSmallint(w,DCsuccscale);
    write_LL_dataunit(c.wdctab,w);   
  end else begin { subsequent approximation DC scan }
    w := p^ shr DCsuccscale;
    writeDirectBits(w,DCsuccbitcount);
  end;
end;


procedure Tjpegstreamwriter.write_interleaved_compo_MCU(row,col : longint; c : Tjpeg_imagecomponent);
var h,v,x,y,k,zrl : longint; p : Pimgdataunit; 
begin
  row := row * c.scanV;
  col := col * c.scanH;
  h := c.scanH-1;
  v := c.scanV-1;
  for y := 0 to v do begin
    for x := 0 to h do begin
      p := c.get_DCT_ZZ_block_pointer(col+x,row+y,0);
      k := spectselstart;
      if (k=0) then begin
        write_DCT_DC_from_pointer(p,c);
        k := k + 1;
      end;
      zrl := 0;
      while (k<=spectselend) do begin
        if (p[k]=0) then begin
          if (k=spectselend) then begin
            writeHuffCodedValue(c.wactab,0); // EOB0
          end else begin
            inc(zrl);
          end;
          inc(k);
        end else begin
          while(zrl>=16) do begin
            writeHuffCodedValue(c.wactab,$F0); // ZRL
            zrl := zrl -16;
          end;
          write_DCT_SEQ_AC_dataunit(c.wactab,zrl,p[k]);
          zrl := 0;
          inc(k);
        end;
      end;
    end;
  end;
end;



procedure Tjpegstreamwriter.write_DCT_interl_MCU(row,col : longint); 
var c : longint;
begin
  for c := 0 to maxcompoinscan do begin
    write_interleaved_compo_MCU(row,col,scancompo[c]);
  end;
  done_mcus(1);
end;




procedure Tjpegstreamwriter.write_DCT_baseline(); 
var row,col : longint;
begin
//writeln('...write_DCT baseline...',maxmcucol+1,'x',maxmcurow+1);
  for row := 0 to maxmcurow do begin
    for col := 0 to maxmcucol do begin
      write_DCT_interl_MCU(row,col);
    end;
  end;
  pad(1);
end;





{

  baseline, the version above ("all but succ ac"): 
  + spectral selection allowed
  + interleaved comopnents allowed
  - EOBRUN not allowed
  + subsequent DC approx allowed (multibit ready)


  succac, the version below
  - spectral selection needed
  - DC-coefficent not allowed (per spec)
  - interleaved comopnents not allowed
  + EOBRUN allowed
  + first+subsequent AC approx allowed

}









procedure Tjpegstreamwriter.scan_DCT(apprlow,apprhi : longint);
var withInterleave,withDC,withAC : boolean;
begin
//writeln('write_DCT... testonly=',testonly);
  startscan(); 
  withdc := (spectselstart=0);
  withac := (spectselend>0)and(spectselstart<=spectselend);
  withInterleave := (maxcompoinscan>0);
  if (apprhi=0) then begin
    DCsuccbitcount := 0;
    DCsuccscale := apprlow;
    if (apprlow=0) then begin
      if not(withdc or withInterleave) then TjpegstreamwriterExcpt.create('nu succ_ac, yet.')
      else write_DCT_baseline()
    end else begin
      if (withInterleave or withDC) then begin
        write_DCT_baseline();
      end else if (withAC) then TjpegstreamwriterExcpt.create('nu succ_ac, yet.')
      else raise TjpegstreamwriterExcpt.create('error with spectral selection specs.');
    end;
  end else begin
    raise TjpegstreamwriterExcpt.create('nu succ_ac, yet.');
  end;
end;



procedure Tjpegstreamwriter.done_mcus(n : longint);
begin
  inc(rstcount,n);
  inc(writtenmcus,n);
  if (rstintervall>0) then begin
    if (rstcount=rstintervall) then begin
      do_restart();
    end;
  end;
end;

procedure Tjpegstreamwriter.do_restart();
begin
  if (writtenmcus<(maxmcurow+1)*(maxmcucol+1)) then begin
    rstcount := 0;
    reset_all_lastDC();
    pad(1);
    if not(testonly) then begin
      dst.writebyte($ff);
      dst.writebyte($d0 + (restartsdone and 7));
      inc(restartsdone);
    end;
  end else begin
    writeln('oh, no restart needed, scan complete.');
  end;
end;



procedure Tjpegstreamwriter.reset_all_lastDC();
var i : longint;
begin
  for i := 0 to high(scancompo) do begin
    scancompo[i].lastDC := inital_lastDC;
  end;
end;

procedure Tjpegstreamwriter.startscan(); 
begin
  writtenmcus := 0;
  rstcount := 0;
  restartsdone := 0;
  reset_all_lastDC();
  maxcompoinscan := high(scancompo);
end;






constructor Tjpegstreamwriter.create();
begin
  inherited create();
  DCsuccbitcount := 0;
  DCsuccscale := 0;
  disable_DC_pred := false;
  rstcount := 0;
  rstintervall := 0;
end;

destructor Tjpegstreamwriter.destroy();
begin
  inherited destroy();
end;

begin
end.

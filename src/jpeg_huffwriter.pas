{$mode objfpc}
unit jpeg_huffwriter;


interface
uses jpeg_huffenc,classes,sysutils, jpeg_misc;

type
Thuffstreamwriter = class
  procedure writebyte(b : byte);
  procedure putBytes();
  procedure writeDirectBits(n,len : dword);
  procedure writeHuffCodedValue(henc : Thuffenc; dat : Thuffdat);
  procedure pad(zeroone : longint);
  procedure setStream(s : Tstream);

  constructor create();
  destructor destroy(); override;


  public
  testonly : boolean; // only forward huffman-usage stats?

  protected
  dst : Tstream;

  private
  bitdat : dword;
  bitsavail : dword;

end;

type ThuffstreamwriterExcpt = TjpegExcpt;


implementation

procedure Thuffstreamwriter.writebyte(b : byte);
begin
  if testonly then exit;
  dst.writeByte(b);
  if (b=255) then dst.writebyte(0);
end;

procedure Thuffstreamwriter.putBytes();
var b : dword;
begin
  if testonly then exit;
  while (bitsavail>=8) do begin
    b := bitdat shr 24;
    bitdat := bitdat shl 8;
    bitsavail := bitsavail - 8;
    writebyte(b);
  end;
end;


procedure Thuffstreamwriter.writeDirectBits(n,len : dword);
begin
  if testonly or (len<=0) then exit;
  putbytes();
  n := n shl (32-len);
  bitdat := bitdat or (n shr bitsavail);
  bitsavail := bitsavail + len;
end;

procedure Thuffstreamwriter.writeHuffCodedValue(henc : Thuffenc; dat : Thuffdat);
begin
  if not(testonly) then begin
    writeDirectBits(henc.getHuffCode(dat),henc.getHuffLen(dat));
  end else begin
    henc.inc_symbstat(dat);
  end;
end;


procedure Thuffstreamwriter.pad(zeroone : longint);
begin
  if testonly then exit;
  putBytes();
  while (bitsavail>0) do begin
    writeDirectBits(zeroone,1);
    putBytes();
  end;
end;

procedure Thuffstreamwriter.setStream(s : Tstream);
begin
  dst := s;
end;

constructor Thuffstreamwriter.create();
begin
  inherited create();
  dst := nil;
  testonly := true;
  bitdat := 0;
  bitsavail := 0;
end;

destructor Thuffstreamwriter.destroy();
begin
  inherited destroy();
  bitsavail := 0;
  dst := nil;
end;

begin
end.

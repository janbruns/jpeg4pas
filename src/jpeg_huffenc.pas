{$mode objfpc}
unit jpeg_huffenc;

{ About generating a huffman table for that can be used with 
  stream encoders.

  The algorithms have mostly already been described in ITU-T.81
}

interface
uses sysutils, jpeg_misc;

type
Tcount = dword;
Thuffdat = byte;
Tbitarr = array of dword;

Thuffenc = class
  constructor create();

  { let the encoder know how much memory is needed
    for the symbols, then increment the symbol-counters
    so that gencode() has the necessary statistics to
    gernerate the code, which will be generated in
    the vars "bits" and "order".
  }
  procedure set_symrange(b : Thuffdat);
  procedure inc_symbstat(b : Thuffdat);
  procedure inc_symbstat(b : Thuffdat; bycnt : longint);
  procedure gencode(maxlen : longint);


{ Now we're ready to build the encoder
  from (bits,order) in the same way as the
  decoder is built.
}
  procedure generate_encoder();

  function getHuffLen(dat : Thuffdat) : dword;
  function getHuffCode(dat : Thuffdat) : dword;


  destructor destroy(); override;


  private
  procedure adjust_to_codelenlimit(maxlen : longint);
  function cmp(a,b : longint) : boolean;
  procedure swp(a,b : longint);
  procedure qsort(a,b : longint);
  procedure register_code(c,cl : dword; dat : Thuffdat);

  public
  huffcode : array of dword;
  hufflen  : array of dword;

  bits : Tbitarr;
  stat : array of Tcount;
  order : array of longint;
  maxsym : Thuffdat;
  symcount : longint;
end;

type Thuffencexcpt = TjpegExcpt;



implementation
uses math;



procedure Thuffenc.set_symrange(b : Thuffdat);
var i : longint;
begin
  maxsym := b;
  symcount := b+2; // include one dummy-sym
  setlength(stat,symcount);
  setlength(order,symcount);
  for i := 0 to symcount-1 do begin
    order[i] := i;
    stat[i] := 0;
  end;
  stat[symcount-1] := 1;
end;





procedure Thuffenc.inc_symbstat(b : Thuffdat);
begin
  if (b>maxsym) then raise Thuffencexcpt.create('inc_symbstat: symbol out of promised bounds.');
  inc(stat[b]);
end;

procedure Thuffenc.inc_symbstat(b : Thuffdat; bycnt : longint);
begin
  if (b>maxsym) then raise Thuffencexcpt.create('inc_symbstat: symbol out of promised bounds.');
  inc(stat[b],bycnt);
end;







function Thuffenc.cmp(a,b : longint) : boolean;
var da,db : longint;
begin
  da := order[a];
  db := order[b];
  cmp := (stat[da]<stat[db]);
end;

procedure Thuffenc.swp(a,b : longint);
var tmp : longint;
begin
  tmp := order[a];
  order[a] := order[b];
  order[b] := tmp;
end;

procedure Thuffenc.qsort(a,b : longint);
var ref,c,d : longint;
begin
  repeat
    c := a;
    d := b;
    ref := (a+b) div 2;
    repeat
      while ( cmp(ref,c) ) do inc(c);
      while ( cmp(d,ref) ) do dec(d);
      if (c<=d) then begin
        swp(c,d);
        if (ref=c) then ref := d 
        else if (ref=d) then ref := c;
        inc(c);
        dec(d);
      end;
    until (c>d);
    if (a<d) then qsort(a,d);
    a := c;
  until (c>=b);
end;







procedure Thuffenc.adjust_to_codelenlimit(maxlen : longint);
var i,j : longint;
begin
  i := high(bits);
  while(i>maxlen) do begin
    if (bits[i]>0) then begin
      j := i-1;
      repeat 
        j := j-1;
      until (bits[j]>0);
      bits[i] := bits[i]-2;
      bits[i-1] := bits[i-1]+1;
      bits[j+1] := bits[j+1]+2;
      bits[j] := bits[j]-1;
    end else i := i-1;
  end;
  while not(bits[i]>0) do dec(i);
  dec(bits[i]);
  setlength(bits,i+1);
end;

procedure Thuffenc.gencode(maxlen : longint);
var wstat : array of Tcount; 
    nxt : array of longint; 
    lvl : array of longint; 
    list : array of longint; 
    syms,liststart,listlen : longint;
    i,e1,e2 : longint;
  procedure putelem(e : longint);
  begin
    if (liststart+listlen>high(list)) then raise Thuffencexcpt.create('temp list out of bounds');
    list[liststart+listlen] := e;
    inc(listlen);
  end;
  function fetchelem() : longint;
  begin
    if (syms>0)and(listlen>0) then begin
      if (wstat[syms-1]<wstat[list[liststart]]) then begin
        fetchelem := syms-1;
        dec(syms);      
      end else begin
        fetchelem := list[liststart];
        inc(liststart);
        dec(listlen);
      end;
    end else if (syms>0) then begin
      fetchelem := syms-1;
      dec(syms);
    end else if (listlen>0) then begin
      fetchelem := list[liststart];
      inc(liststart);
      dec(listlen);
    end else raise Thuffencexcpt.create('fetcherror!');
  end;
begin
//  writeln('before sort');
//  for i := 0 to symcount-1 do writeln(i:4,order[i]:4,' : ',stat[order[i]]);

  qsort(0,symcount-1);
  while(symcount>1)and(stat[order[symcount-1]]=0) do dec(symcount);
  if (symcount<2) then raise Thuffencexcpt.create('empty symbol-list');
  setlength(wstat,symcount);
  for i := 0 to symcount-1 do wstat[i] := stat[order[i]];
  setlength(lvl,symcount);
  for i := 0 to symcount-1 do lvl[i] := 0;
  setlength(nxt,symcount);
  for i := 0 to symcount-1 do nxt[i] := -1;
  setlength(list,symcount);

//  writeln('after sort2');
//  for i := 0 to symcount-1 do writeln(i:4,order[i]:4,lvl[i]:4,' : ',stat[order[i]]);

  syms := symcount;
  listlen := 0;
  liststart := 0;
  while ((listlen>1)or(syms>0)) do begin
    e1 := fetchelem();
    e2 := fetchelem();
    wstat[e1] := wstat[e2] + wstat[e1];
    putelem(e1);
    repeat
      inc(lvl[e1]);
      if (nxt[e1]>=0) then e1 := nxt[e1] else break;
    until false;
    nxt[e1] := e2;
    repeat
      inc(lvl[e2]);
      if (nxt[e2]>=0) then e2 := nxt[e2] else break;
    until false;
  end;
  e1 := 0;
  for i := 0 to symcount-1 do e1 := max(e1,lvl[i]);
  setlength(bits,e1+1);
  for i := 0 to e1 do bits[i] := 0;
  for i := 0 to symcount-1 do begin
    if (lvl[i]>=0)and(lvl[i]<=high(bits)) 
    then inc(bits[lvl[i]])
    else write('?####?');
  end;
  adjust_to_codelenlimit(maxlen);
  // the sort might have moved the dummy symbol elsewhere...
  e1 := maxsym+1;
  for i := symcount-2 downto 0  do begin
    if (order[i]=e1)  then begin
      order[i] := order[symcount-1];
      break;
    end;
  end;

  setlength(order,symcount-1);
//  writeln('after gen');
//  for i := 0 to high(order) do writeln(i:4,order[i]:4,lvl[i]:4,' : ',stat[order[i]]);
  setlength(stat,0);
end;








function Thuffenc.getHuffLen(dat : Thuffdat) : dword;
begin
  if (dat>maxsym) then raise Thuffencexcpt.create('getHuffLen: symbol '+inttostr(dat)+' out of promised bounds.');
  getHuffLen := hufflen[dat];
end;

function Thuffenc.getHuffCode(dat : Thuffdat) : dword;
begin
  if (dat>maxsym) then raise Thuffencexcpt.create('getHuffCode: symbol out of promised bounds.');
  getHuffCode := huffcode[dat];
end;

procedure Thuffenc.register_code(c,cl : dword; dat : Thuffdat);
begin
  huffcode[dat] := c shr (32-cl);
  hufflen[dat] := cl;
end;

procedure Thuffenc.generate_encoder();
var i,j,maxi,n : longint; c,rc : dword;
begin
  maxi := 0; n := -1;
  for i := 1 to high(bits) do begin
    if (bits[i]>0) then maxi := i;
    n := n + bits[i];
  end;
  if (n>high(order)) then raise Thuffencexcpt.create('More code-points ('+inttostr(n+1)+')than user-values('+inttostr(high(order)+1)+') requested.');
  if (n<high(order)) then raise Thuffencexcpt.create('Less code-points ('+inttostr(n+1)+')than user-values('+inttostr(high(order)+1)+') requested.');

  maxsym := 0; 
  j := high(order);
  for i := 0 to j do begin
    maxsym := max(maxsym,order[i]);
  end;
  setlength(huffcode,maxsym+1);
  setlength(hufflen ,maxsym+1);

  c := 0; n := 0;
  for i := 1 to maxi do begin
    for j := bits[i] downto 1 do begin
//writeln(binstr(c,i),' : ',order[n]);
      rc := c shl (32-i);
      register_code(rc,i,order[n]);
      inc(c); inc(n);
    end;
    c := c shl 1;
  end;
end;





constructor Thuffenc.create();
begin
end;

destructor Thuffenc.destroy();
begin
end;





begin

end.


